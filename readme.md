# G&G X App #

![](icon_128.png)

## How to Use

You can use this package as a go module and extend it.

```go

func TestApp(t *testing.T) {
// init environment 
gg.Paths.SetWorkspacePath("./_workspace")
gg.Paths.SetTempRoot(gg.Paths.WorkspacePath("/temp"))

// creates application
app, err := ggxapp.NewApplicationBuilder("debug")
if nil != err {
t.Error(err)
t.FailNow()
}

// override default settings and ensure http and database are enabled
app.Settings().Serve.Http = true
app.Settings().Serve.Database = true
err = app.SaveSettings()
if nil != err {
t.Error(err)
t.FailNow()
}

// START APP
runtime, err := app.Build()
if nil != err {
t.Error(err)
t.FailNow()
}

// expose API
runtime.Http().RegisterNoAuth("get", "/api/v1/cmd/custom", custom.CmdCustom) // no http authentication
runtime.Http().RegisterAuth("get", "/api/v1/cmd/custom_auth", custom.CmdCustom) // http authentication
// add custom handlers
runtime.Command().SetExecutor(custom.NewExecCustom())

err = runtime.Start()
if nil != err {
t.Error(err)
t.FailNow()
}

// wait forever
runtime.Join()
}
```

## API DOCS ##

[API documentation exported from Postman](_docs/GGX-APP.postman_collection.json)

To use just call:

```
go get bitbucket.org/digi-sense/gg-xapp@latest
```

### Versioning ###

Sources are versioned using git tags:

```
git tag v0.2.23
git push origin v0.2.23
```