package custom

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcontroller/ggxcommand"
	"fmt"
	"github.com/gofiber/fiber/v2"
)

const CmdCustom = "cmd_custom"

type ExecCustom struct {
	ctrl *ggxcommand.GGxCommands
}

func NewExecCustom() (instance *ExecCustom) {
	instance = new(ExecCustom)

	return
}

func (instance *ExecCustom) Close() {}

func (instance *ExecCustom) SetController(ctrl *ggxcommand.GGxCommands) ggxcommand.IExecutor {
	instance.ctrl = ctrl
	return instance
}

func (instance *ExecCustom) IsHandler(commandName string) bool {
	return gg.Arrays.IndexOf(commandName, []string{
		CmdCustom,
	}) > -1
}

func (instance *ExecCustom) Execute(sender interface{}, command *ggxcommand.ApiStatement) (response interface{}, err error) {
	switch command.Statement {
	case CmdCustom:
		// get the context
		ctx := command.Params["_ctx"].(*fiber.Ctx)
		ip := ctx.IP() // get IP of request
		req := ctx.Request()
		userAgent := string(req.Header.UserAgent())

		// the response
		response = map[string]interface{}{
			"text":       "HELLO, THIS IS A CUSTOM HANDLER.",
			"params":     command.Params,
			"ip":         ip,
			"user-agent": userAgent,
		}
		return
	}
	return nil, gg.Errors.Prefix(ggxcommons.ExecutionError,
		fmt.Sprintf("Command not supported: '%s'", command.Statement))
}
