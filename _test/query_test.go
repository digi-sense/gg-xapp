package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database/query"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database_pool"
	"fmt"
	"testing"
)

const config = "{\"uid\": \"main\",\"driver\": \"sqlite\",\"dsn\": \"./main.db\"}"

func TestQueryUserGetNotConfirmed(t *testing.T) {
	root := gg.Paths.Absolute("./_workspace/database")
	pool := ggxserve_database_pool.NewPool("debug", root, nil)
	pool.AddConnection(config)
	q, err := query.NewUserQuery(pool)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	userQ := q.(*query.UserQuery)
	list, err := userQ.UsersListGetNotVerified(0)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(list)
	for _, item := range list {
		userQ.Delete(item, item.ID == 4)
	}
}
