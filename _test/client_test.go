package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp_client"
	"fmt"
	"testing"
	"time"
)

const (
	host     = "http://localhost:9090"
	username = "angelo.geminiani@gmail.com"
	password = "123456"
)

func TestUserGet(t *testing.T) {
	client, err := ggxapp_client.NewClientConnection(host, username, password)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	// return curren logged-in user
	response, err := client.AuthUserGet()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(gg.JSON.Stringify(response))
}

func TestUserPost(t *testing.T) {
	client, err := ggxapp_client.NewClientConnection(host, username, password)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	params := map[string]interface{}{
		"id": 1,
		"payload": map[string]interface{}{
			"timestamp": time.Now(),
		},
	}
	response, err := client.UserUpdate(params)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(gg.JSON.Stringify(response))
}
