package _test

import (
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_webserver"
	"fmt"
	"testing"
)

func TestGetPath(t *testing.T) {
	path := ggxserve_webserver.GetPathNoParams("/api/v1/sys/get.user/:userID")
	fmt.Println(path)
}
