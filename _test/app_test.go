package _test

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-xapp/_test/custom"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxpostman"
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"strings"
	"testing"
)

func TestApp(t *testing.T) {
	// init environment (ONLY FOR TEST)
	gg.Paths.SetWorkspacePath("./_workspace")
	gg.Paths.SetTempRoot(gg.Paths.WorkspacePath("/temp"))

	ggxapp.SetAppName("TEST APP")
	ggxapp.SetAppVersion("0.0.1b")

	// creates application
	app, err := ggxapp.NewApplicationBuilder("debug", "./_workspace")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// override default settings and ensure http and database are enabled
	app.Settings().Serve.Webserver = true
	app.Settings().Serve.Database = true
	err = app.SaveSettings()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// START APP
	runtime, err := app.Build()
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	// expose API ggxapp.Runtime.Webserver()
	runtime.Webserver().RegisterNoAuth("get", "/api/v1/cmd/custom", custom.CmdCustom)    // no http authentication
	runtime.Webserver().RegisterAuth("get", "/api/v1/cmd/custom_auth", custom.CmdCustom) // http authentication
	runtime.Webserver().RegisterAuth("get", "/api/v1/cmd/custom2/:param1", custom.CmdCustom)
	runtime.Webserver().Handle("get", "/api/v1/custom/test", func(ctx *fiber.Ctx) error {
		html, err := gg.IO.ReadTextFromFile("./file.html")
		if nil != err {
			return ggxapp.WebResponseWrite(ctx, nil, err)
		} else {
			return ggxapp.WebResponseWriteHtml(ctx, html)
		}
	})
	runtime.Webserver().Handle("get", "/api/v1/template/:name", func(ctx *fiber.Ctx) error {
		params := ggxapp.WebGetParams(ctx, true)
		name := gg.Maps.GetString(params, "name")
		if len(name) == 0 {
			return ggxapp.WebResponseWrite(ctx, nil, errors.New("Missing param 'name'. Path=/api/v1/template/:name"))
		} else {
			html := runtime.Templates().Get("html", name)
			if len(html) == 0 {
				html, err := gg.IO.ReadTextFromFile("./file-not-found.html")
				if nil != err {
					return ggxapp.WebResponseWrite(ctx, nil, err)
				} else {
					return ggxapp.WebResponseWriteHtml(ctx, html)
				}
			} else {
				return ggxapp.WebResponseWriteHtml(ctx, html)
			}
		}
	})
	runtime.Webserver().Handle("post", "/api/v1/files/upload", func(ctx *fiber.Ctx) error {
		// params := ggxapp.WebGetParams(ctx, true)
		form, e := ctx.MultipartForm()
		if nil == e {
			files, e := ggxapp.WebUploadMultipart(ctx, form, "./uploads", false, 0, "")
			if nil != e {
				return e
			}
			fmt.Println(files)
			return ggxapp.WebResponseWrite(ctx, strings.Join(files, ","), nil)
		}
		return nil
	})

	// add txt files to whitelist
	ggxapp.WebUploadAddToWhitelistExt("txt")

	// add custom handlers
	runtime.Command().SetExecutor(custom.NewExecCustom())

	err = runtime.Start()

	// upload templates
	err = app.Templates().DeployEmailTemplate("email", "mytpl", "Test", "<html/>", "Hola")
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	text := app.Templates().Get("email", "mytpl.html")
	if text != "<html/>" {
		t.Error("Expected <html/>")
		t.FailNow()
	}

	// test templates
	text = app.Templates().Get("email", "welcome.html")
	fmt.Println("Template 'email.welcome.html' len=", len(text))

	// FROM app.Build() YOU CAN USE ALSO "ggxapp.Runtime" VARIABLE

	// send email using postman
	m := ggxpostman.NewGGxPostmanTemplateMessage("welcome", nil, "angelo.geminiani@gmail.com")
	m.Bcc = []string{"angelo.geminiani+1@gmail.com"}
	m.Cc = []string{"angelo.geminiani+2@gmail.com"}
	m.Attachments = []interface{}{gg.Paths.Absolute("./file.html")}
	ggxapp.Runtime.Events().EmitAsync(ggxapp.EventOnSendEmail, m)

	// wait forever
	ggxapp.Runtime.Join()
}

func launchClientUploader() {

}
