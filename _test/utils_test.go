package _test

import (
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"fmt"
	"testing"
	"time"
)

func TestEncryptDecript(t *testing.T) {
	fmt.Println("now", time.Now())
	ggxcommons.Salt = "this.is.a.salt"
	duration := 60 * 60 * 24 * 365 * 100 * time.Second
	token, err := ggxcommons.TokenCreate("1234", map[string]interface{}{
		"fld1": "hello",
	}, duration)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	//token := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyX2lkIjoiMSIsInBheWxvYWQiOnsidWlkIjoxfSwiZXhwIjoxNzA3ODU4Nzc1LCJqdGkiOiI0NDViOTBiOWI4OTc4YjBlMjliZTU0YWE5MDk5MTQ0YyJ9.3GESefLvXrLj5rdcMOfcZTEcdlSJK3Q3EAOuV_zLGs4"
	valid, err := ggxcommons.TokenValidate(token)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("valid", valid, token)

	fmt.Println(token)
	enc := ggxcommons.Encrypt(token)
	token = ggxcommons.Decrypt(enc)
	fmt.Println(token)

	date, err := ggxcommons.TokenExpireDate(token)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("date", date)
}

func TestAccessToken(t *testing.T) {
	fmt.Println("now", time.Now())
	token, err := ggxcommons.TokenCreate("1234", map[string]interface{}{
		"fld1": "hello",
	}, 2*time.Second)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println(token)
	valid, err := ggxcommons.TokenValidate(token)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("valid", valid)

	payload, err := ggxcommons.TokenPayload(token)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("payload", payload)

	date, err := ggxcommons.TokenExpireDate(token)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("date", date)

	time.Sleep(3 * time.Second)

	valid, err = ggxcommons.TokenValidate(token)
	if nil == err {
		t.Error("Expected not valid token")
		t.FailNow()
	}
	fmt.Println("valid", valid)
}

func TestAccessTokenWithEncrypt(t *testing.T) {
	ggxcommons.Salt = "this.is.a.salt"

	duration := 60 * 24 * 365 * 100 * time.Second
	// duration := 3*time.Second

	token, err := ggxcommons.TokenCreate("1234", map[string]interface{}{
		"fld1": "hello",
	}, duration)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}

	fmt.Println(token)
	enc := ggxcommons.Encrypt(token)
	token = ggxcommons.Decrypt(enc)
	fmt.Println(token)

	valid, err := ggxcommons.TokenValidate(token)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("valid", valid)

	payload, err := ggxcommons.TokenPayload(token)
	if nil != err {
		t.Error(err)
		t.FailNow()
	}
	fmt.Println("payload", payload)

}

func TestGetLink(t *testing.T) {
	link := "/path/url/:param1/:param2/:param3"
	params := map[string]interface{}{
		"param1": "p1",
		"param2": "p2",
		"param3": "p3",
	}
	response := ggxcommons.GetLink(link, params)
	fmt.Println(response)

	link = "/path/url/:param1"
	response = ggxcommons.GetLink(link, "p1")
	fmt.Println(response)
}
