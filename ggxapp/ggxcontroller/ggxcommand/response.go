package ggxcommand

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"errors"
	"fmt"
)

type ApiResponse struct {
	Referral  string                `json:"referral"`   // channel who called the api. values: "https", "email"
	Sender    string                `json:"sender"`     // user who called the api. Email or user ID
	UserId    uint                  `json:"user_id"`    // identified user
	UserEmail string                `json:"user_email"` // identified user
	Command   *ApiStatement         `json:"command"`    // original command sent
	Response  *ApiStatementResponse `json:"response"`   // command response
}

func (instance *ApiResponse) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *ApiResponse) SetResult(response interface{}, err error) {
	if nil == instance.Response {
		instance.Response = new(ApiStatementResponse)
	}
	if nil != err {
		instance.Response.Error = ggxcommons.ExecutionError.Error()
		instance.Response.ErrorMessage = err.Error()
	} else {
		instance.Response.Result = response
	}
}

type ApiStatement struct {
	Statement string                 `json:"statement"`
	Params    map[string]interface{} `json:"params"`
	Payload   map[string]interface{} `json:"payload"`
}

type ApiStatementResponse struct {
	Error        string      `json:"error"`
	ErrorMessage string      `json:"error_message"`
	Result       interface{} `json:"result"`
}

func (instance *ApiStatementResponse) IsError() bool {
	return len(instance.Error) > 0 || len(instance.ErrorMessage) > 0
}

func (instance *ApiStatementResponse) GetError() error {
	if nil != instance && instance.IsError() {
		if len(instance.Error) > 0 {
			return errors.New(fmt.Sprintf("%s:%s", instance.Error, instance.ErrorMessage))
		}
		return errors.New(fmt.Sprintf("%s", instance.ErrorMessage))
	}
	return nil
}

func (instance *ApiStatementResponse) SetResult(response interface{}, err error) {
	if nil != err {
		instance.Error = ggxcommons.ExecutionError.Error()
		instance.ErrorMessage = err.Error()
	} else {
		instance.Result = response
	}
}
