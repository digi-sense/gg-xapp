package ggxcommand

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database"
	"fmt"
)

type GGxCommands struct {
	mode        string
	logger      gg_log.ILogger
	events      *gg_events.Emitter
	database    *ggxserve_database.GGxDatabaseController
	getAuthUser ggxcommons.GetAuthUserFunc

	executors  []IExecutor
	baseApiURL string
	baseAppURL string
}

func NewGGxCommands(logger gg_log.ILogger, events *gg_events.Emitter, database *ggxserve_database.GGxDatabaseController) (instance *GGxCommands) {
	instance = new(GGxCommands)
	instance.logger = logger
	instance.events = events
	instance.database = database

	instance.executors = make([]IExecutor, 0)

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxCommands) Close() {
	if nil != instance {
		for _, item := range instance.executors {
			item.Close()
		}
	}
}

func (instance *GGxCommands) SetBaseURLs(baseApiURL, baseAppURL string) {
	if nil != instance {
		instance.baseApiURL = baseApiURL
		instance.baseAppURL = baseAppURL
	}
}

func (instance *GGxCommands) BaseAppURL() string {
	if nil != instance {
		return instance.baseAppURL
	}
	return ""
}

func (instance *GGxCommands) BaseApiURL() string {
	if nil != instance {
		return instance.baseApiURL
	}
	return ""
}

func (instance *GGxCommands) Database() *ggxserve_database.GGxDatabaseController {
	if nil != instance {
		return instance.database
	}
	return nil
}

func (instance *GGxCommands) Logger() gg_log.ILogger {
	if nil != instance {
		return instance.logger
	}
	return nil
}

func (instance *GGxCommands) Events() *gg_events.Emitter {
	if nil != instance {
		return instance.events
	}
	return nil
}

func (instance *GGxCommands) SetAuthenticator(userFunc ggxcommons.GetAuthUserFunc) {
	if nil != instance {
		instance.getAuthUser = userFunc
	}
}

func (instance *GGxCommands) Execute(referral, sender, command string, params, payload map[string]interface{}) (response *ApiResponse) {
	statement := new(ApiStatement)
	statement.Statement = command
	statement.Params = params
	statement.Payload = payload

	return instance.executeCommand(referral, sender, statement)
}

// ---------------------------------------------------------------------------------------------------------------------
//	r e g i s t e r
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxCommands) SetExecutor(executor IExecutor) {
	if nil != instance {
		executor.SetController(instance)
		instance.executors = append(instance.executors, executor)
	}
}

func (instance *GGxCommands) GetExecutor(commandName string) (IExecutor, error) {
	if nil != instance {
		for _, item := range instance.executors {
			if item.IsHandler(commandName) {
				return item, nil
			}
		}
		return nil, gg.Errors.Prefix(ggxcommons.ExecutionError, fmt.Sprintf("Missing executor for command : '%s'", commandName))
	}
	return nil, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxCommands) executeCommand(referral, authId string, command *ApiStatement) (response *ApiResponse) {
	response = new(ApiResponse)
	response.Referral = referral
	response.Sender = authId
	response.Command = command
	response.Response = new(ApiStatementResponse)

	var user ggxcommons.IAuthUser
	var err error
	if len(authId) > 0 && nil != instance.getAuthUser {
		// should check user is registered and enabled to run command
		user, err = instance.getAuthUser(authId)
		if nil == err {
			if nil == user || !user.HasAuthForCommands(referral) {
				err = gg.Errors.Prefix(ggxcommons.AuthorizationError, "User is not authorized: ")
			}
		}
		if nil != err {
			response.Response.Error = ggxcommons.AuthorizationError.Error()
			response.Response.ErrorMessage = err.Error()
			return
		}

		response.UserId = uint(user.GetId())
		response.UserEmail = user.GetEmail()
	}

	// get executor
	executor, err := instance.GetExecutor(command.Statement)
	if nil != err {
		response.Response.Error = ggxcommons.AuthorizationError.Error()
		response.Response.ErrorMessage = err.Error()
		return
	}

	response.SetResult(executor.Execute(user, command))

	return
}

func (instance *GGxCommands) execute(commandName string, params map[string]interface{}) (interface{}, error) {
	executor, err := instance.GetExecutor(commandName)
	if nil != err {
		return nil, err
	}
	statement := new(ApiStatement)
	statement.Statement = commandName
	statement.Params = params
	return executor.Execute(nil, statement)
}
