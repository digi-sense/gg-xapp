package ggxcommand

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode"
	"bitbucket.org/digi-sense/gg-core-x/gg_2dcode/commons"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"fmt"
)

const (
	CmdSysVersion = ggxcommons.CmdSysVersion
	CmdSysQrCode  = ggxcommons.CmdSysQrCode
)

type ExecSys struct {
	ctrl *GGxCommands
}

func NewExecSys() (instance *ExecSys) {
	instance = new(ExecSys)

	return
}

func (instance *ExecSys) Close() {
	if nil != instance {

	}
}

func (instance *ExecSys) SetController(ctrl *GGxCommands) IExecutor {
	instance.ctrl = ctrl
	return instance
}

func (instance *ExecSys) IsHandler(commandName string) bool {
	return gg.Arrays.IndexOf(commandName, []string{
		CmdSysVersion,
		CmdSysQrCode,
	}) > -1
}

func (instance *ExecSys) Execute(sender interface{}, command *ApiStatement) (response interface{}, err error) {
	switch command.Statement {
	case CmdSysVersion:
		return fmt.Sprintf("%s:%s", gg.AppName, gg.AppVersion), nil
	case CmdSysQrCode:
		return instance.createQrCode(command.Params)
	}
	return nil, gg.Errors.Prefix(ggxcommons.ExecutionError,
		fmt.Sprintf("Command not supported: '%s'", command.Statement))
}

func (instance *ExecSys) createQrCode(params map[string]interface{}) (bytes []byte, err error) {
	if nil != instance {
		content := gg.Maps.GetString(params, "content")
		if len(content) > 0 {
			generator := gg_2dcode.NewGenerator(commons.BarcodeFormat_QR_CODE)
			return generator.Encode(content,
				&commons.OptionSize{Width: 300},
				// &commons.OptionCircleShapes{},
				// &commons.OptionImageLogo{Filename: "./icon.png"}
			)
		} else {
			err = gg.Errors.Prefix(ggxcommons.ExecutionError, "Missing param 'content'")
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------
