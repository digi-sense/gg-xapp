package ggxcommand

import (
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database"
)

type IExecutor interface {
	Close()
	IsHandler(commandName string) bool
	Execute(sender interface{}, command *ApiStatement) (response interface{}, err error)
	SetController(ctrl *GGxCommands) IExecutor
}

type CommandExecutor struct {
	database *ggxserve_database.GGxDatabaseController
	events   *gg_events.Emitter
}

func NewCommandExecutor(database *ggxserve_database.GGxDatabaseController, logger *ggxcommons.Logger, events *gg_events.Emitter) *CommandExecutor {
	instance := new(CommandExecutor)
	instance.database = database
	instance.events = events
	instance.init()
	return instance
}

func (instance *CommandExecutor) IsHandler(commandName string) bool {
	return false
}

func (instance *CommandExecutor) Execute(referral, sender, command string, params map[string]interface{}) (response *ApiResponse) {
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *CommandExecutor) init() {

}
