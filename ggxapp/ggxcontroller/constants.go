package ggxcontroller

// ---------------------------------------------------------------------------------------------------------------------
//	r o l e s
// ---------------------------------------------------------------------------------------------------------------------

const (
	RoleApiAll          = "api-all"         // all api enabled
	RoleApiEmail        = "api-email"       // enable email commands
	RoleApiWeb          = "api-web"         // enable web api
	RoleApiCreateUser   = "api-create-user" // enable user creation
	RoleApiRemoveUser   = "api-remove-user" // enable user remove
	RoleApiAdminAll     = "api-admin"
	RoleApiAdminAccount = "api-admin-account"
)
