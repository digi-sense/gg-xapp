package ggxapp

import (
	"bitbucket.org/digi-sense/gg-core"
	ggx "bitbucket.org/digi-sense/gg-core-x"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-core/gg_promise"
	"bitbucket.org/digi-sense/gg-core/gg_state"
	"bitbucket.org/digi-sense/gg-core/gg_stoppable"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"bitbucket.org/digi-sense/gg-core/gg_xtend"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxconfig"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcontroller/ggxcommand"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxinitializer"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxpostman"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database/query"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_templates"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_webserver"
	"fmt"
	"mime"
	"path/filepath"
	"time"
)

//----------------------------------------------------------------------------------------------------------------------
//	runtime
//----------------------------------------------------------------------------------------------------------------------

type GGxApplicationRuntime struct {
	application *GGxApplicationBuilder
}

func (instance *GGxApplicationRuntime) Mode() string {
	if nil != instance && nil != instance.application {
		return instance.application.Mode()
	}
	return ""
}

func (instance *GGxApplicationRuntime) DirWork() string {
	if nil != instance && nil != instance.application {
		return instance.application.DirWork()
	}
	return ""
}

func (instance *GGxApplicationRuntime) Modules() *ApplicationModules {
	if nil != instance && nil != instance.application {
		return instance.application.Modules()
	}
	return nil
}

func (instance *GGxApplicationRuntime) Xtend() *gg_xtend.XtendHelper {
	if nil != instance && nil != instance.application {
		return instance.application.Xtend()
	}
	return nil
}

func (instance *GGxApplicationRuntime) Start() (err error) {
	if nil != instance.application.fileMonitors {
		for _, fm := range instance.application.fileMonitors {
			fm.Start()
		}
	}

	if nil != instance.application.webserver {
		instance.application.webserver.Start()
	}

	if nil != instance.application.modules {
		_ = instance.application.modules.Start()
	}

	// TODO: add here more start commands

	// write log report
	instance.logStartReport()

	// on started logs
	instance.application.events.Emit(ggxcommons.EventOnStarted)

	// send log to database if enabled
	instance.application.events.EmitAsync(ggxcommons.EventOnLog,
		ggxcommons.NewLogInfo(fmt.Sprintf("Server started: %s", gg.Formatter.FormatDate(time.Now(), "yyyy-MM-dd HH:mm:ss"))))

	// start the stoppable helper
	instance.application.stoppable.Start()

	return
}

func (instance *GGxApplicationRuntime) Stop() {
	if nil != instance && nil != instance.application {
		Runtime = nil
		instance.application.stoppable.Stop()
	}
}

func (instance *GGxApplicationRuntime) Join() {
	if nil != instance.application.stoppable {
		instance.application.stoppable.Join()
	}
}

func (instance *GGxApplicationRuntime) OnStop(callback func()) *GGxApplicationRuntime {
	if nil != instance && nil != instance.application {
		instance.application.OnStop(callback)
	}
	return instance
}

//----------------------------------------------------------------------------------------------------------------------
//	expose controllers to expand
//----------------------------------------------------------------------------------------------------------------------

// State global state manager and controllers container
// Add and Get objects from state
// Emits EventOnChangeState when changed
func (instance *GGxApplicationRuntime) State() *gg_state.State {
	if nil != instance && nil != instance.application {
		return instance.application.State()

	}
	return nil
}

func (instance *GGxApplicationRuntime) Logger() gg_log.ILogger {
	if nil != instance && nil != instance.application {
		return instance.application.logger
	}
	return nil
}

func (instance *GGxApplicationRuntime) Webserver() *ggxserve_webserver.GGxWebController {
	if nil != instance && nil != instance.application {
		return instance.application.webserver
	}
	return nil
}

func (instance *GGxApplicationRuntime) Database() *ggxserve_database.GGxDatabaseController {
	if nil != instance && nil != instance.application {
		return instance.application.database
	}
	return nil
}

func (instance *GGxApplicationRuntime) Command() *ggxcommand.GGxCommands {
	if nil != instance && nil != instance.application {
		return instance.application.command
	}
	return nil
}

func (instance *GGxApplicationRuntime) Events() *gg_events.Emitter {
	if nil != instance && nil != instance.application {
		return instance.application.events
	}
	return nil
}

func (instance *GGxApplicationRuntime) Delegator() *gg_promise.Delegator {
	if nil != instance && nil != instance.application {
		return instance.application.delegator
	}
	return nil
}

func (instance *GGxApplicationRuntime) Initializer() *ggxinitializer.GGxInitializer {
	if nil != instance && nil != instance.application {
		return instance.application.initializer
	}
	return nil
}

func (instance *GGxApplicationRuntime) Postman() *ggxpostman.GGxPostman {
	if nil != instance && nil != instance.application {
		return instance.application.postman
	}
	return nil
}

func (instance *GGxApplicationRuntime) PostmanFromSettings(config *ggxconfig.SettingsPostman) *ggxpostman.GGxPostman {
	if nil != instance && nil != instance.application &&
		nil != instance.application.logger && nil != instance.application.events &&
		nil != instance.application.templates {
		return ggxpostman.NewGGxPostmanWithSettings(config,
			instance.application.logger,
			instance.application.events,
			instance.application.templates)
	}
	return nil
}

func (instance *GGxApplicationRuntime) Templates() *ggxserve_templates.GGxTemplates {
	if nil != instance && nil != instance.application {
		return instance.application.templates
	}
	return nil
}

func (instance *GGxApplicationRuntime) logStartReport() {
	logger := instance.Logger()
	logger.Info(fmt.Sprintf("RUN '%s' v%s, mode='%s', Workspace='%s'", GetAppName(), GetAppVersion(), instance.Mode(), instance.DirWork()))
	logger.Info("=================================")

	userHome, _ := gg.Paths.UserHomeDir()
	logger.Info(fmt.Sprintf(" USER INFO: home=%s, name=%s", userHome, gg.Paths.FileName(userHome, false)))
	info := gg.Sys.GetInfo()
	logger.Info(fmt.Sprintf(" SYSTEM INFO: name=%v, kernel=%v, platform=%v, cpu=%v", info.Hostname, info.Kernel, info.Platform, info.CPUs))
	logger.Info(fmt.Sprintf(" MEMORY INFO: %v", info.MemoryUsage))
	process, e := gg.Sys.FindCurrentProcess()
	if nil == e {
		logger.Info(fmt.Sprintf(" CURRENT PROCESS PID: %v", process.Pid))
	}

	logger.Info(" ---------------------------------")
	logger.Info(" MIME TEST:")
	logger.Info(fmt.Sprintf("   js:   %s", mime.TypeByExtension(".js")))
	logger.Info(fmt.Sprintf("   css:  %s", mime.TypeByExtension(".css")))
	logger.Info(fmt.Sprintf("   html: %s", mime.TypeByExtension(".html")))
	logger.Info(fmt.Sprintf("   json: %s", mime.TypeByExtension(".json")))
	logger.Info(" ---------------------------------")

	// libs info
	logger.Info(fmt.Sprintf(" gg-core version: %v", gg.Version))
	logger.Info(fmt.Sprintf(" gg-core-x version: %v", ggx.Version))

	// xapp version
	logger.Info(fmt.Sprintf(" gg-xapp version: %v", ggxcommons.LibVersion))

	// modules info
	logger.Info(fmt.Sprintf(" gg-xapp-modules info: %v", instance.Modules().Info()))

	logger.Info("=================================")
}

//----------------------------------------------------------------------------------------------------------------------
//	application builder
//----------------------------------------------------------------------------------------------------------------------

type GGxApplicationBuilder struct {
	mode    string
	root    string
	dirWork string

	fileMonitors []*FileMonitor
	modules      *ApplicationModules
	xtend        *gg_xtend.XtendHelper

	initializer *ggxinitializer.GGxInitializer
	logger      gg_log.ILogger
	stoppable   *gg_stoppable.Stoppable
	settings    *ggxconfig.Settings
	state       *gg_state.State
	events      *gg_events.Emitter
	delegator   *gg_promise.Delegator

	// controllers
	authConfig *ggxconfig.Authorization
	database   *ggxserve_database.GGxDatabaseController
	command    *ggxcommand.GGxCommands
	webserver  *ggxserve_webserver.GGxWebController
	templates  *ggxserve_templates.GGxTemplates
	postman    *ggxpostman.GGxPostman
}

func NewApplicationBuilder(mode, dirWork string) (*GGxApplicationBuilder, error) {
	return NewApplicationBuilderWithLogRotator(mode, dirWork, true)
}

func NewApplicationBuilderWithLogRotator(mode, dirWork string, allowLogRotate bool) (instance *GGxApplicationBuilder, err error) {
	instance = new(GGxApplicationBuilder)
	instance.mode = mode
	instance.dirWork = dirWork
	instance.fileMonitors = make([]*FileMonitor, 0)
	instance.modules = NewApplicationModules()
	instance.xtend = new(gg_xtend.XtendHelper)

	// initialize settings files
	err = instance.init(allowLogRotate)

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *GGxApplicationBuilder) Mode() string {
	if nil != instance {
		return instance.mode
	}
	return ""
}

func (instance *GGxApplicationBuilder) DirWork() string {
	if nil != instance {
		return instance.dirWork
	}
	return ""
}

func (instance *GGxApplicationBuilder) Modules() *ApplicationModules {
	if nil != instance {
		return instance.modules
	}
	return nil
}

func (instance *GGxApplicationBuilder) Xtend() *gg_xtend.XtendHelper {
	if nil != instance {
		return instance.xtend
	}
	return nil
}

func (instance *GGxApplicationBuilder) Logger() gg_log.ILogger {
	if nil != instance {
		return instance.logger
	}
	return nil
}

func (instance *GGxApplicationBuilder) LoggerRotateEnable(value bool) *GGxApplicationBuilder {
	if logger, ok := instance.logger.(*ggxcommons.Logger); ok {
		logger.RotateEnable(value)
	}
	return instance
}

func (instance *GGxApplicationBuilder) LoggerRotateMaxSizeMb(value float64) *GGxApplicationBuilder {
	if nil != instance {
		if logger, ok := instance.logger.(*ggxcommons.Logger); ok {
			logger.RotateMaxSizeMb(value)
		}
	}
	return instance
}

func (instance *GGxApplicationBuilder) Settings() *ggxconfig.Settings {
	if nil != instance && nil != instance.settings {
		return instance.settings
	}
	return new(ggxconfig.Settings)
}

func (instance *GGxApplicationBuilder) SaveSettings() (err error) {
	if nil != instance && nil != instance.settings {
		filename := gg.Paths.WorkspacePath(fmt.Sprintf("settings.%s.json", instance.mode))
		_, err = gg.IO.WriteTextToFile(instance.settings.String(), filename)
	}
	return
}

func (instance *GGxApplicationBuilder) Initializer() *ggxinitializer.GGxInitializer {
	if nil != instance {
		return instance.initializer
	}
	return nil
}

func (instance *GGxApplicationBuilder) State() *gg_state.State {
	if nil != instance {
		return instance.state
	}
	return nil
}

func (instance *GGxApplicationBuilder) OnStop(callback func()) *GGxApplicationBuilder {
	if nil != instance && nil != instance.stoppable {
		instance.stoppable.OnStop(callback)
	}
	return instance
}

func (instance *GGxApplicationBuilder) AddFileMonitor(name string, handler func(event *gg_events.Event), moreDirs ...string) *GGxApplicationBuilder {
	if nil != instance {
		dirs := []string{instance.root, instance.dirWork}
		if len(moreDirs) > 0 {
			dirs = append(dirs, moreDirs...)
		}
		eventName := fmt.Sprintf("on_monitor_%s", name)
		fm := NewFileMonitor(dirs, name, eventName, instance.events)
		instance.fileMonitors = append(instance.fileMonitors, fm)
		instance.events.On(eventName, handler)
	}
	return instance
}

func (instance *GGxApplicationBuilder) Templates() *ggxserve_templates.GGxTemplates {
	if nil != instance {
		return instance.templates
	}
	return nil
}

func (instance *GGxApplicationBuilder) Build() (runtime *GGxApplicationRuntime, err error) {
	runtime = new(GGxApplicationRuntime)
	runtime.application = instance
	Runtime = runtime

	if nil != instance.settings && nil != instance.settings.Serve {

		// creates database folder because is used from auth
		_ = gg.Paths.Mkdir(gg.Paths.WorkspacePath("database") + gg_utils.OS_PATH_SEPARATOR)

		// DATABASE: database connection and query manager
		if instance.settings.Serve.Database {
			instance.database, err = ggxserve_database.NewDatabaseController(instance.mode, instance.logger, instance.events)
			if nil != err {
				goto exit
			}
			// init query controllers
			// this can be done also from application controller
			if !instance.database.HasQueryController(query.NameQueryLog) {
				instance.database.SetQueryController(query.NameQueryLog, query.NewLogQuery)
			}
			//if !instance.database.HasQueryController(query.NameQueryUser) {
			//	instance.database.SetQueryController(query.NameQueryUser, query.NewUserQuery)
			//}

			// COMMANDS: command executors
			instance.command = ggxcommand.NewGGxCommands(instance.logger, instance.events, instance.database)
			instance.command.SetExecutor(ggxcommand.NewExecSys())
			// instance.command.SetExecutor(ggxcommand.NewExecUser(instance.events))
		}

		// WEBSERVER: webserver
		baseApiURL, baseAppURL := "", ""
		if instance.settings.Serve.Webserver {
			instance.webserver = ggxserve_webserver.NewWebController(instance.mode,
				instance.logger,
				instance.events, instance.command)
			if api := instance.webserver.SettingsApi(); nil != api {
				baseApiURL = api.ApiBaseUrl
				baseAppURL = api.AppBaseUrl
			}
			instance.authConfig = instance.webserver.SettingsAuthorization()

			// inject baseAPiURL and baseAppURL
			if nil != instance.command {
				instance.command.SetBaseURLs(baseApiURL, baseAppURL)
			}
		}

		// TEMPLATES: manage html, email and sms templates
		if instance.settings.Serve.Templates {
			instance.templates = ggxserve_templates.NewGGxTemplates(baseApiURL, baseAppURL, instance.events, instance.initializer)
			if nil != instance.webserver {
				// inject template engine into webserver
				instance.webserver.SetTemplateEngine(instance.templates)
			}
		}

		// POSTMAN: send emails and SMS (require templates for email templates)
		instance.postman = ggxpostman.NewGGxPostman(instance.mode,
			instance.logger, instance.events, instance.templates)

	} // settings

	// EXIT point
exit:
	if nil != err {
		instance.logger.Error("=================================")
		instance.logger.Error(fmt.Sprintf(" ERROR starting '%s' v%s: %s",
			gg.AppName, gg.AppVersion, err.Error()))
		instance.logger.Info("=================================")
	}

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *GGxApplicationBuilder) init(allowLogRotate bool) (err error) {
	// paths
	instance.dirWork = gg.Paths.Absolute(instance.dirWork) // "absolutize"
	instance.root = filepath.Dir(instance.dirWork)

	// ensure entire workspace is set
	gg.Paths.GetWorkspace(ggxcommons.WpDirWork).SetPath(instance.dirWork)

	// initialize settings files
	instance.initializer = ggxinitializer.NewGGxInitializer()
	err = instance.initializer.Initialize(instance.mode)
	if nil != err {
		return
	}

	instance.state = gg_state.NewState(gg.AppName)
	instance.events = instance.state.Events()

	instance.delegator = gg_promise.NewDelegator()

	// add default file monitor: STOP file
	instance.AddFileMonitor("stop", instance.onDoStop)
	// creates stoppable helper
	instance.stoppable = gg_stoppable.NewStoppable()
	instance.stoppable.SetLogger(instance.logger)
	instance.stoppable.AddStopOperation("file-monitors", instance.shutdownFileMonitors)
	instance.stoppable.AddStopOperation("http-server", instance.shutdownWebserver)
	instance.stoppable.AddStopOperation("modules", instance.shutdownModules)

	// logger as first parameter
	// l := gg.Arrays.GetAt(args, 0, nil)
	instance.logger = ggxcommons.NewLogger(instance.mode, nil, allowLogRotate)

	// load settings
	filename := gg.Paths.WorkspacePath(fmt.Sprintf("settings.%s.json", instance.mode))
	err = gg.JSON.ReadFromFile(filename, &instance.settings)
	if nil == err {
		ggxcommons.AppSettings = instance.settings
	}

	return
}

func (instance *GGxApplicationBuilder) onDoStop(_ *gg_events.Event) {
	if nil != instance {
		// STOPPED BY FILE
		instance.stoppable.Stop()
	}
}

func (instance *GGxApplicationBuilder) shutdownFileMonitors() (err error) {
	if nil != instance {
		// stop all monitors
		if nil != instance.fileMonitors {
			for _, fm := range instance.fileMonitors {
				fm.Stop()
			}
		}
	}
	return
}

func (instance *GGxApplicationBuilder) shutdownWebserver() (err error) {
	if nil != instance {
		if nil != instance.webserver {
			instance.webserver.Stop()
		}
	}
	return
}

func (instance *GGxApplicationBuilder) shutdownModules() (err error) {
	if nil != instance {
		if nil != instance.modules {
			instance.modules.Stop()
		}
	}
	return
}
