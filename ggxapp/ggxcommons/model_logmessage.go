package ggxcommons

import "bitbucket.org/digi-sense/gg-core"

type LogMessage struct {
	UserId  uint   `json:"user-id"`
	DbUid   string `json:"db-uid"` // tenant database. If empty the log is stored on main-log database
	Message string `json:"message"`
	Level   string `json:"level"`
	Error   error  `json:"error"`
	Payload string `json:"payload"`
}

func NewLogInfo(message string, payload ...interface{}) *LogMessage {
	spayload := ""
	if len(payload) > 0 {
		spayload = gg.Convert.ToString(payload)
	}
	return &LogMessage{
		UserId:  0,
		Message: message,
		Level:   "info",
		Error:   nil,
		Payload: spayload,
	}
}

func NewLogError(err error, payload ...interface{}) *LogMessage {
	spayload := ""
	if len(payload) > 0 {
		spayload = gg.Convert.ToString(payload)
	}
	return &LogMessage{
		UserId:  0,
		Message: "",
		Level:   "error",
		Error:   err,
		Payload: spayload,
	}
}
