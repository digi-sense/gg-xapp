package ggxcommons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxconfig"
	"errors"
)

const (
	LibVersion = "0.2.23"

	ModeProduction = gg.ModeProduction
	ModeDebug      = gg.ModeDebug

	DbLog  = "log"
	DbMain = "main"

	LogCategorySystem = "system"
	LogCategoryUser   = "user"
)

const (
	PatternDate = "MM-dd-yyyy HH:mm"
)

var (
	AppName = "New Application"

	Salt = "" // enable/disable encryption of sensible data

	AppSettings *ggxconfig.Settings
)

// ---------------------------------------------------------------------------------------------------------------------
//	w o r k s p a c e s
// ---------------------------------------------------------------------------------------------------------------------

const (
	WpDirStart = "start"
	WpDirApp   = "app"
	WpDirWork  = "*"
)

// ---------------------------------------------------------------------------------------------------------------------
//	e r r o r s
// ---------------------------------------------------------------------------------------------------------------------

var (
	PanicSystemError   = gg.PanicSystemError
	RuntimeError       = errors.New("error_runtime")
	AuthorizationError = errors.New("authorization_error")
	ExecutionError     = errors.New("execution_error")

	UserAlreadyRegisteredError = errors.New("user_already_registered") // 401
	UserEmailError             = errors.New("user_email_error")        // 500
	UserNotFoundError          = errors.New("invalid_credentials")     // 401
	UserPermissionDeniedError  = errors.New("permission_denied")       // 403
	UserAuthDataError          = errors.New("user_auth_data_error")    // 401
	UserNotConfirmedError      = errors.New("user_not_confirmed")      // 401
	UserDataError              = errors.New("user_data_error")         // 500
	MissingParamsError         = errors.New("missing_params")          // 500
	MismatchParamsError        = errors.New("mismatch_params_error")   // 500
	ChannelNotFoundError       = errors.New("channel_not_found")       // 500
)

var (
	HttpUnauthorizedError       = errors.New("unauthorized")          // 401
	HttpInvalidCredentialsError = errors.New("invalid_credentials")   // 401
	AccessTokenExpiredError     = errors.New("access_token_expired")  // 403
	RefreshTokenExpiredError    = errors.New("refresh_token_expired") // 401
	AccessTokenInvalidError     = errors.New("access_token_invalid")  // 401
	HttpUnsupportedApiError     = errors.New("unsupported_api")
)

// ---------------------------------------------------------------------------------------------------------------------
//		e v e n t s
// ---------------------------------------------------------------------------------------------------------------------

const (
	EventOnDoStop  = "on_do_stop"
	EventOnStarted = "on_started"

	EventOnLog  = "on_log"  // log event
	EventOnStat = "on_stat" // statistics event

	EventOnSendEmail = "on_send_email"
	EventOnSendSms   = "on_send_sms"

	EventOnSignup     = "on_signup"
	EventOnSignin     = "on_signin"
	EventOnGetToken   = "on_get_token"
	EventOnUserUpdate = "on_user_update"
)
