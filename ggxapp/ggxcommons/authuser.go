package ggxcommons

type IAuthUser interface {
	GetId() int
	HasAuthForCommands(role string) bool
	GetEmail() string
}

type GetAuthUserFunc func(identifier string) (IAuthUser, error)
