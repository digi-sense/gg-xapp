package ggxcommons

import "time"

type StatMessage struct {
	UserId    uint                   `json:"user-id"`
	Timestamp time.Time              `json:"timestamp"`
	Payload   map[string]interface{} `json:"payload"`
}

func NewStatMessage(user uint, payload map[string]interface{}) (instance *StatMessage) {
	instance = new(StatMessage)
	instance.UserId = user
	instance.Timestamp = time.Now()
	instance.Payload = payload
	if nil == instance.Payload {
		instance.Payload = make(map[string]interface{})
	}
	return
}
