package ggxcommons

import "strings"

const (
	CmdSysVersion = "sys_version"
	CmdSysQrCode  = "sys_qrcode"
)

// ---------------------------------------------------------------------------------------------------------------------
//	a p i
// ---------------------------------------------------------------------------------------------------------------------

//-- API SYS --//

var (
	ApiSysVersion = "/api/v1/" + strings.Replace(CmdSysVersion, "_", "/", 1)
	ApiSysQrCode  = "/api/v1/" + strings.Replace(CmdSysQrCode, "_", "/", 1)
)
