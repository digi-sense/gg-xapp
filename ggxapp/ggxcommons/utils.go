package ggxcommons

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_auth0"
	"bitbucket.org/digi-sense/gg-core-x/gg_auth0/jwt"
	"bitbucket.org/digi-sense/gg-core-x/gg_auth0/jwt/elements"
	"bitbucket.org/digi-sense/gg-core-x/gg_auth0/jwt/signing"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"fmt"
	"net/url"
	"strings"
	"time"
)

var tokenSignature = "token-signature"

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

// Recover error recover function
// pass arguments like logger and/or context message
func Recover(args ...interface{}) {
	gg.Recover(args...)
}

func GetLink(link string, data interface{}) (response string) {
	response = link
	if confirmToken, ok := data.(string); ok {
		fieldName := gg.Arrays.GetAt(strings.Split(response, ":"), 1, "")
		response = "." + strings.ReplaceAll(response, fmt.Sprintf(":%v", fieldName), url.QueryEscape(confirmToken))
	}
	if params, ok := data.(map[string]interface{}); ok {
		fields := strings.Split(link, ":")
		for _, field := range fields {
			field = strings.ReplaceAll(field, "/", "")
			if v, b := params[field]; b {
				response = strings.ReplaceAll(response, fmt.Sprintf(":%v", field), url.QueryEscape(gg.Convert.ToString(v)))
			}
		}
		response = "." + response
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	internal tokens
// ---------------------------------------------------------------------------------------------------------------------

func TokenSetKey(newSignature string){
	tokenSignature = newSignature
}

func TokenCreate(uid interface{}, payload map[string]interface{}, duration time.Duration) (signed string, err error) {
	key := []byte(tokenSignature)
	if nil == payload {
		payload = make(map[string]interface{})
	}
	payload["uid"] = uid
	claims := new(gg_auth0.Auth0Claims)
	claims.UserId = gg.Convert.ToString(uid)
	claims.Payload = payload
	claims.Id = gg.Coding.MD5(fmt.Sprintf("%v-%v", key, uid))

	claims.ExpiresAt = time.Now().Add(duration).Unix()

	token := jwt.NewWithClaims(signing.SigningMethodHS256, claims)
	signed, err = token.SignedString(key)

	return
}

func TokenValidate(stringToken string) (bool, error) {
	// parse the token
	token, err := parseToken(stringToken)
	if nil != err {
		return false, err
	}
	if !token.Valid {
		return false, AccessTokenInvalidError
	}
	return true, nil
}

func TokenParse(stringToken string) (response map[string]interface{}, err error) {
	// parse the token
	var token *elements.Token
	token, err = parseToken(stringToken)
	if nil != err {
		return nil, err
	}
	if !token.Valid {
		err = AccessTokenInvalidError
		return
	}

	claims := token.GetMapClaims()
	response = map[string]interface{}{}
	setPayload(response, claims)

	return
}

func TokenPayload(stringToken string) (payload map[string]interface{}, err error) {
	// parse the token
	var token *elements.Token
	token, err = parseToken(stringToken)
	if nil != err {
		return
	}
	if !token.Valid {
		err = AccessTokenInvalidError
	} else {
		p := token.GetMapClaims()["payload"]
		payload, _ = p.(map[string]interface{})
	}
	return
}

func TokenExpireDate(stringToken string) (expDate time.Time, err error) {
	// parse the token
	var token map[string]interface{}
	token, err = TokenParse(stringToken)
	if nil != err {
		return
	}

	exp := gg.Reflect.GetInt(token, "exp")
	expDate = time.Unix(int64(exp), 0)

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	settings
// ---------------------------------------------------------------------------------------------------------------------

func GetFilenameSettings(mode string) string {
	return fmt.Sprintf("settings.%s.json", mode)
}

// ---------------------------------------------------------------------------------------------------------------------
//	d a t a b a s e
// ---------------------------------------------------------------------------------------------------------------------

func IsRecordNotFoundError(err error) bool {
	return nil != err && err.Error() == "record not found"
}

func MD5(value string) string {
	return gg.Coding.MD5(strings.ToLower(value))
}

func Encrypt(value string) string {
	if len(Salt) > 0 && len(value) > 0 {
		value = gg.Coding.UnwrapBase64(value)
		enc, err := gg.Coding.EncryptTextWithPrefix(value, []byte(Salt))
		if nil == err {
			enc = gg.Coding.WrapBase64(enc)
			return enc
		}
	}
	return value
}

func Decrypt(value string) string {
	if len(Salt) > 0 && len(value) > 0 {
		value = gg.Coding.UnwrapBase64(value)
		response, err := gg.Coding.DecryptTextWithPrefix(value, []byte(Salt))
		if nil == err {
			return response
		}
	}
	return value
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func parseToken(stringToken string) (*elements.Token, error) {
	return jwt.Parse(stringToken, func(token *elements.Token) (interface{}, error) {
		// secretType := token.GetMapClaims()["secret_type"].(string)
		accessKey := []byte(tokenSignature)
		if len(accessKey) == 0 {
			return nil, AccessTokenInvalidError
		}
		return accessKey, nil
	})
}

func setPayload(target map[string]interface{}, source map[string]interface{}) {
	if nil != target {
		for k, v := range source {
			if m, b := v.(map[string]interface{}); b {
				setPayload(target, m)
			} else {
				if gg.Arrays.IndexOf(k, gg_auth0.PROTECTED_FIELDS) == -1 {
					target[k] = v
				}
			}
		}
	}
}

func getMessage(r interface{}, args ...interface{}) (response string) {
	method := ""
	for _, item := range args {
		if s, ok := item.(string); ok {
			method = s
			break
		}
	}
	if len(method) > 0 {
		response = fmt.Sprintf("Error in application '%s' on '%s': %s", gg.AppName, method, r)
	} else {
		response = fmt.Sprintf("[panic] Generic error in application '%s': %s", gg.AppName, r)
	}
	return
}

func getLogger(args ...interface{}) (response gg_log.ILogger) {
	for _, item := range args {
		if logger, ok := item.(gg_log.ILogger); ok {
			response = logger
			break
		}
	}
	return
}
