package ggxcommons

import (
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxconfig"
	"github.com/gofiber/fiber/v2"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type IWebsecure interface {
	IsEnabled() bool
	AuthenticateRequest(ctx *fiber.Ctx, assert bool, auth *ggxconfig.Authorization) bool
	GetAuthId(ctx *fiber.Ctx) string
}
