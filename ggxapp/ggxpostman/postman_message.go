package ggxpostman

type GGxPostmanMessage struct {
	Provider    string        `json:"provider"`
	From        string        `json:"from"`
	To          []string      `json:"to"`
	Bcc         []string      `json:"bcc"`
	Cc          []string      `json:"cc"`
	Subject     string        `json:"subject"`
	Text        string        `json:"text"`
	Html        string        `json:"html"`
	Attachments []interface{} `json:"attachments"`

	TemplateName    string                 `json:"template_name"`
	TemplatePayload map[string]interface{} `json:"template_payload"`
}

func NewGGxPostmanTemplateMessage(name string, payload map[string]interface{}, to ...string) *GGxPostmanMessage {
	instance := new(GGxPostmanMessage)
	instance.TemplateName = name
	instance.TemplatePayload = payload
	instance.To = append(make([]string, 0), to...)
	return instance
}

func NewGGxPostmanMessage(subject, text, html string, to ...string) *GGxPostmanMessage {
	instance := new(GGxPostmanMessage)
	instance.Subject = subject
	instance.Text = text
	instance.Html = html
	instance.To = append(make([]string, 0), to...)
	return instance
}
