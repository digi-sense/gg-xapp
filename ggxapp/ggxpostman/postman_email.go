package ggxpostman

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_exec_bucket"
	"fmt"
	"strings"
)

type GGxPostmanEmailSender struct {
	mailHost    string
	mailPort    int
	mailSecure  bool
	mailUser    string
	mailPass    string
	mailFrom    string
	mailReplyTo string

	bucket   *gg_exec_bucket.BucketExec
	filename string
}

func NewPostmanEmailSender(config map[string]interface{}) (instance *GGxPostmanEmailSender) {
	instance = new(GGxPostmanEmailSender)
	instance.mailHost = gg.Reflect.GetString(config, "host")
	instance.mailPort = gg.Reflect.GetInt(config, "port")
	instance.mailSecure = gg.Reflect.GetBool(config, "secure")
	instance.mailUser = gg.Reflect.GetString(config, "user")
	instance.mailPass = gg.Reflect.GetString(config, "pass")
	instance.mailFrom = gg.Reflect.GetString(config, "from")
	instance.mailReplyTo = gg.Reflect.GetString(config, "reply_to")

	if len(instance.mailFrom) == 0 {
		instance.mailFrom = instance.mailUser
	}

	// fallback bucket that uses a py script to send email if other methods fail.
	// this is useful for GMail SMTP over TLS
	gg.Bucket.SetRoot(gg.Paths.WorkspacePath("./"))
	instance.bucket = gg.Bucket.NewBucket("python3", true)

	instance.filename = gg.Paths.WorkspacePath("./scripts/send_email.py")

	return
}

func (instance *GGxPostmanEmailSender) Send(to, bcc, cc, subject, contentText, contentHtml string, attachments ...interface{}) (err error) {
	err = gg.Email.SendMessage(instance.mailHost, instance.mailPort, instance.mailSecure,
		instance.mailUser, instance.mailPass, instance.mailFrom, instance.mailReplyTo,
		to, bcc, cc, subject, contentText, contentHtml, attachments)
	if nil != err {
		e := instance.fallbackSend(to, bcc, cc, subject, contentText, contentHtml, attachments...)
		if nil != e {
			err = gg.Errors.Prefix(e, fmt.Sprintf("Multiple errors. 1->'%s', 2->", err))
		} else {
			err = nil
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxPostmanEmailSender) fallbackSend(to, bcc, cc, subject, contentText, contentHtml string, attachments ...interface{}) (err error) {
	if nil != instance && nil != instance.bucket {
		if ok, e := gg.Paths.Exists(instance.filename); ok {
			program, perr := instance.bucket.NewProgram(instance.filename)
			if nil != perr {
				err = perr
			} else {
				model := map[string]interface{}{
					"user":     instance.mailUser,
					"password": instance.mailPass,
					"host":     instance.mailHost,
					"port":     instance.mailPort,
					"from":     instance.mailFrom,
					"reply_to": instance.mailReplyTo,

					"to":          to,
					"bcc":         bcc,
					"cc":          cc,
					"subject":     strings.Trim(subject, "\n "),
					"text":        contentText,
					"html":        contentHtml,
					"attachments": strings.Join(gg.Convert.ToArrayOfString(attachments), ","),
				}
				err = program.Merge(model)
				if nil == err {
					task := program.Run()
					_, _, err = task.Wait()
				}
			}
		} else {
			err = e
		}
	}
	return
}
