package ggxpostman

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x"
	"bitbucket.org/digi-sense/gg-core-x/gg_sms"
	"bitbucket.org/digi-sense/gg-core-x/gg_sms/gg_sms_engine"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxconfig"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_templates"
	"errors"
	"fmt"
	"strings"
)

var (
	EmptyTemplateError = errors.New("empty-template")

	// extensions = []string{"txt", "htm", "html"}
	dirEmail = "email"
	dirSms   = "sms"
	dirHtml  = "html"
)

// ---------------------------------------------------------------------------------------------------------------------
//	t y p e
// ---------------------------------------------------------------------------------------------------------------------

type GGxPostman struct {
	mode      string
	uid       string
	logger    gg_log.ILogger
	events    *gg_events.Emitter
	templates *ggxserve_templates.GGxTemplates // may be null

	config  *ggxconfig.SettingsPostman
	payload map[string]interface{}

	sms   *gg_sms.SMSEngine
	email *GGxPostmanEmailSender
}

func NewGGxPostman(mode string, logger gg_log.ILogger, events *gg_events.Emitter, templates *ggxserve_templates.GGxTemplates) *GGxPostman {
	config, err := loadSettings("postman", mode)
	if nil == err {
		return NewGGxPostmanWithSettings(config, logger, events, templates)
	}
	return nil
}

func NewGGxPostmanWithSettings(config *ggxconfig.SettingsPostman, logger gg_log.ILogger, events *gg_events.Emitter, templates *ggxserve_templates.GGxTemplates) *GGxPostman {
	instance := new(GGxPostman)
	instance.logger = logger
	instance.events = events
	instance.templates = templates

	instance.payload = map[string]interface{}{}

	if nil != config {
		instance.uid = config.Uid
		instance.config = config

		// init payload
		if nil != config.Payload {
			instance.payload = gg.Maps.Merge(true, instance.payload, config.Payload)
		}

		// initialize SMS channel
		if nil != config.ConfigSms {
			smsConfig, e := gg_sms_engine.NewSMSConfigurationFromMap(config.ConfigSms)
			if nil == e {
				instance.sms = ggx.SMS.NewEngine(smsConfig)
			}
		}

		// initialize EMAIL channel
		if nil != config.ConfigMail {
			instance.email = NewPostmanEmailSender(config.ConfigMail)
		}

		// init listeners
		instance.events.On(ggxcommons.EventOnSendSms, instance.onSendSMS)
		instance.events.On(ggxcommons.EventOnSendEmail, instance.onSendEmail)
	}

	if len(instance.uid) == 0 {
		instance.uid = gg.Rnd.Uuid()
	}

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxPostman) GetUid() string {
	if nil != instance {
		return instance.uid
	}
	return ""
}

func (instance *GGxPostman) SetUid(value string) *GGxPostman {
	if nil != instance {
		instance.uid = value
	}
	return instance
}

func (instance *GGxPostman) Payload() map[string]interface{} {
	if nil != instance {
		return instance.payload
	}
	return nil
}

func (instance *GGxPostman) GetHtmlTemplate(name string, payload map[string]interface{}) (content string, err error) {
	payload = instance.data(payload)
	content, err = instance.content(dirHtml, fmt.Sprintf("%v", name), payload)
	return
}

func (instance *GGxPostman) GetHtmlTemplateFromDir(dirName, name string, payload map[string]interface{}) (content string, err error) {
	payload = instance.data(payload)
	content, err = instance.content(dirName, fmt.Sprintf("%v", name), payload)
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	E M A I L
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxPostman) SendEmail(to, bcc, cc, subject, content string, attachments ...interface{}) (err error) {
	contentText := ""
	contentHtml := ""
	if gg.Regex.IsHTML(content) {
		contentHtml = content
	} else {
		contentText = content
	}
	err = instance.SendEmailFull(to, bcc, cc, subject, contentText, contentHtml, attachments...)
	return
}

func (instance *GGxPostman) SendEmailFull(to, bcc, cc, subject, contentText, contentHtml string, attachments ...interface{}) (err error) {
	if nil != instance && nil != instance.email {
		err = instance.email.Send(to, bcc, cc, subject, contentText, contentHtml, attachments...)
	}
	return
}

// SendEmailTemplate usage: SendEmailTemplate ("verify", "address@info.com", {"user":"Mario"})
func (instance *GGxPostman) SendEmailTemplate(name, to, bcc, cc string, payload map[string]interface{}, attachments ...interface{}) (err error) {
	return instance.SendEmailTemplateFromDir(dirEmail, name, to, bcc, cc, payload, attachments...)
}

func (instance *GGxPostman) SendEmailTemplateFromDir(dirName, name, to, bcc, cc string, payload map[string]interface{}, attachments ...interface{}) (err error) {
	if nil != instance && nil != instance.email {
		payload = instance.data(payload)
		var contentHtml, contentText string
		contentHtml, err = instance.content(dirName, fmt.Sprintf("%v.html", name), payload)
		if nil != err {
			return
		}
		contentText, err = instance.content(dirName, fmt.Sprintf("%v.text", name), payload)
		if nil != err {
			return
		}
		subject := instance.subject(dirName, name, payload)

		err = instance.email.Send(to, bcc, cc, subject, contentText, contentHtml, attachments...)
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	S M S
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxPostman) SendSMS(providerName, content, to, from string) (response string, err error) {
	response, err = instance.sms.SendMessage(providerName, content, to, from)
	return
}

func (instance *GGxPostman) SendSmsTemplate(name, to string, payload map[string]interface{}) (response string, err error) {
	return instance.SendSmsTemplateFromDir(dirSms, name, to, payload)
}

func (instance *GGxPostman) SendSmsTemplateFromDir(dirName, name, to string, payload map[string]interface{}) (response string, err error) {
	payload = instance.data(payload)
	var content string
	content, err = instance.content(dirName, name, payload)
	if nil != err {
		return
	}

	response, err = instance.SendSMS("", content, to, "")

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxPostman) subject(dir, name string, payload map[string]interface{}) string {
	name = fmt.Sprintf("%s.subject", name)
	content, _ := instance.content(dir, name, payload)
	if len(content) == 0 {
		content = "Email"
	}
	return content
}

func (instance *GGxPostman) content(dir, name string, payload map[string]interface{}) (content string, err error) {
	content, err = instance.templates.Render(dir, name, payload)
	if nil != err {
		return
	}

	if len(content) == 0 {
		err = gg.Errors.Prefix(EmptyTemplateError, fmt.Sprintf("%v/%v", dir, name))
	}
	return
}

func (instance *GGxPostman) data(payload map[string]interface{}) map[string]interface{} {
	// creates data with payload
	data := map[string]interface{}{}
	for k, v := range instance.payload {
		data[k] = v
	}
	data = gg.Maps.Merge(true, data, payload)
	return data
}

func (instance *GGxPostman) onSendSMS(event *gg_events.Event) {
	rawMessage := event.Argument(0)
	if message, ok := rawMessage.(*GGxPostmanMessage); ok {
		if len(message.TemplateName) > 0 {
			for _, to := range message.To {
				_, _ = instance.SendSmsTemplate(message.TemplateName, to, message.TemplatePayload)
			}
		} else {
			for _, to := range message.To {
				_, _ = instance.SendSMS("", message.Text, to, message.From)
			}
		}
	}
}

func (instance *GGxPostman) onSendEmail(event *gg_events.Event) {
	rawMessage := event.Argument(0)
	if message, ok := rawMessage.(*GGxPostmanMessage); ok {
		errors := make([]string, 0)
		bcc := strings.Join(message.Bcc, ",")
		cc := strings.Join(message.Cc, ",")
		if len(message.TemplateName) > 0 {
			for _, to := range message.To {
				err := instance.SendEmailTemplate(message.TemplateName, to, bcc, cc, message.TemplatePayload, message.Attachments...)
				if nil != err {
					errors = append(errors, err.Error())
				}
			}
		} else {
			for _, to := range message.To {
				err := instance.SendEmailFull(to, bcc, cc, message.Subject, message.Text, message.Html)
				if nil != err {
					errors = append(errors, err.Error())
				}
			}
		}
		if len(errors) > 0 {
			instance.logger.Error(fmt.Sprintf("Error sending email in GGxPostman: %s", strings.Join(errors, ", ")))
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func loadSettings(name, mode string) (*ggxconfig.SettingsPostman, error) {
	path := gg.Paths.WorkspacePath(name + "." + mode + ".json")
	settings := new(ggxconfig.SettingsPostman)
	text, err := gg.IO.ReadTextFromFile(path)
	if nil != err {
		return settings, err
	}
	err = gg.JSON.Read(text, &settings)
	return settings, err
}
