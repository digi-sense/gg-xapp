package ggxconfig

import "bitbucket.org/digi-sense/gg-core"

type Settings struct {
	Serve *SettingsServe `json:"serve"`
}

func (instance *Settings) String() string {
	return gg.JSON.Stringify(instance)
}

type SettingsServe struct {
	Database  bool `json:"database"`
	Webserver bool `json:"webserver"` // enable/disable HTTP server
	Templates bool `json:"templates"`
}
