package ggxconfig

import (
	"bitbucket.org/digi-sense/gg-core"
)

type AuthApi struct {
	Enabled         bool                    `json:"enabled"`
	AppBaseUrl      string                  `json:"app-base-url"`
	ApiBaseUrl      string                  `json:"api-base-url"`
	RoutingOverride *AuthApiSettingsRouting `json:"routing-override"`
}

func (instance *AuthApi) ContainsRouteFor(uid string) (*AuthApiSettingsRoute, bool) {
	if nil != instance.RoutingOverride {
		return instance.RoutingOverride.Contains(uid)
	}
	return nil, false
}

type WebserverSettings struct {
	Enabled bool                   `json:"enabled"`
	Http    map[string]interface{} `json:"http"`
	Auth    *Authorization         `json:"auth"`
	Api     *AuthApi               `json:"auth-api"`
}

func (instance *WebserverSettings) ToString() string {
	return gg.JSON.Stringify(instance)
}

func (instance *WebserverSettings) ToMap() map[string]interface{} {
	return gg.Convert.ToMap(instance)
}

type AuthApiSettingsRoute struct {
	Method         string         `json:"method"`
	Endpoint       string         `json:"endpoint"`
	Authorization  *Authorization `json:"authorization"`
	ParamsRequired []string       `json:"params-required"`
	Params         []string       `json:"params"`
}

type AuthApiSettingsRouting struct {
	m map[string]*AuthApiSettingsRoute

	AuthSignIn                *AuthApiSettingsRoute `json:"sign-in"`   // login
	AuthSignUp                *AuthApiSettingsRoute `json:"sign-up"`   // new user
	AuthUserGet               *AuthApiSettingsRoute `json:"user-get"`  // get user by access_token
	AuthUserPost              *AuthApiSettingsRoute `json:"user-post"` // update user data (not password+email)
	AuthVerify                *AuthApiSettingsRoute `json:"verify"`    // confirm account
	AuthForgotPassword        *AuthApiSettingsRoute `json:"forgot-password"`
	AuthChangePassword        *AuthApiSettingsRoute `json:"change-password"`
	AuthResetPassword         *AuthApiSettingsRoute `json:"reset-password"`
	AuthResendActivationEmail *AuthApiSettingsRoute `json:"resend-activation-email"`
	AuthRemove                *AuthApiSettingsRoute `json:"remove"`
	AuthImport                *AuthApiSettingsRoute `json:"import"`
	AuthValidateToken         *AuthApiSettingsRoute `json:"validate-token"`
	AuthRefreshToken          *AuthApiSettingsRoute `json:"refresh-token"`
	AuthGrantDelegation       *AuthApiSettingsRoute `json:"delegate-grant"`
	AuthRevokeDelegation      *AuthApiSettingsRoute `json:"delegate-revoke"`
}

func (instance *AuthApiSettingsRouting) ToMap() map[string]*AuthApiSettingsRoute {
	if nil == instance.m {
		m := gg.Convert.ToMap(instance)
		instance.m = make(map[string]*AuthApiSettingsRoute)
		for k, v := range m {
			var route *AuthApiSettingsRoute
			err := gg.JSON.Read(gg.JSON.Stringify(v), &route)
			if nil == err {
				instance.m[k] = route
			}
		}
	}
	return instance.m
}

func (instance *AuthApiSettingsRouting) Contains(name string) (*AuthApiSettingsRoute, bool) {
	if item, ok := instance.ToMap()[name]; ok {
		return item, true
	}
	return nil, false
}
