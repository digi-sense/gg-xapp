package ggxconfig

import "bitbucket.org/digi-sense/gg-core-x/gg_auth0"

type Authorization struct {
	Type  string `json:"type"`
	Value string `json:"value"`
}

type SettingsAuth0 struct {
	gg_auth0.Auth0Config
	PasswordExpireDays            int `json:"password-expire-days"`
	AccountNotConfirmedExpireDays int `json:"account-not-confirmed-expire-days"`
	AccessTokenExpireSeconds      int `json:"access-token-expire-seconds"`
}
