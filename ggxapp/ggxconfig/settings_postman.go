package ggxconfig

type SettingsPostman struct {
	Uid        string                 `json:"uid"`
	Payload    map[string]interface{} `json:"payload"`
	ConfigMail map[string]interface{} `json:"config-mail"`
	ConfigSms  map[string]interface{} `json:"config-sms"`
}
