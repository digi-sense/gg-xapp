package ggxapp

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-core/gg_promise"
	"bitbucket.org/digi-sense/gg-core/gg_state"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcontroller"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_webserver"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"mime/multipart"
	"strings"
	"time"
)

func init() {
	Models = new(ModelsHelper)
	Events = new(EventsHelper)
	Delegator = new(DelegatorHelper)
}

// ---------------------------------------------------------------------------------------------------------------------
//	c o n s t a n t s
// ---------------------------------------------------------------------------------------------------------------------

const (
	LibVersion = ggxcommons.LibVersion

	PatternDate = ggxcommons.PatternDate // "MM-dd-yyyy HH:mm"

	ModeProduction = gg.ModeProduction
	ModeDebug      = gg.ModeDebug

	EventOnDoStop      = ggxcommons.EventOnDoStop
	EventOnStarted     = ggxcommons.EventOnStarted
	EventOnLog         = ggxcommons.EventOnLog
	EventOnSendEmail   = ggxcommons.EventOnSendEmail
	EventOnSendSms     = ggxcommons.EventOnSendSms
	EventOnChangeState = gg_state.EventOnChangeState
)

const (
	RoleApiAll        = ggxcontroller.RoleApiAll        // all api enabled
	RoleApiEmail      = ggxcontroller.RoleApiEmail      // enable email commands
	RoleApiWeb        = ggxcontroller.RoleApiWeb        // enable web api
	RoleApiCreateUser = ggxcontroller.RoleApiCreateUser // enable user creation
	RoleApiRemoveUser = ggxcontroller.RoleApiRemoveUser // enable user remove

	RoleApiAdminAll     = ggxcontroller.RoleApiAdminAll
	RoleApiAdminAccount = ggxcontroller.RoleApiAdminAccount
)

var (
	RuntimeError       = ggxcommons.RuntimeError
	PanicSystemError   = ggxcommons.PanicSystemError
	AuthorizationError = ggxcommons.AuthorizationError
	ExecutionError     = ggxcommons.ExecutionError

	MissingParamsError  = ggxcommons.MissingParamsError  // 500
	MismatchParamsError = ggxcommons.MismatchParamsError // 500
)

// ---------------------------------------------------------------------------------------------------------------------
//	global runtime
// ---------------------------------------------------------------------------------------------------------------------

// Runtime
// public variable for quick access to application runtime
var Runtime *GGxApplicationRuntime

// ---------------------------------------------------------------------------------------------------------------------
//	helpers
// ---------------------------------------------------------------------------------------------------------------------

var Models *ModelsHelper

type ModelsHelper struct {
}

func (instance *ModelsHelper) NewStatMessage(user uint, payload map[string]interface{}) *ggxcommons.StatMessage {
	return ggxcommons.NewStatMessage(user, payload)
}

func (instance *ModelsHelper) NewLogInfo(message string, payload ...interface{}) *ggxcommons.LogMessage {
	return ggxcommons.NewLogInfo(message, payload)
}

func (instance *ModelsHelper) NewLogError(err error, payload ...interface{}) *ggxcommons.LogMessage {
	return ggxcommons.NewLogError(err, payload)
}

// ---------------------------------------------------------------------------------------------------------------------
//	event helpers
// ---------------------------------------------------------------------------------------------------------------------

var Events *EventsHelper

type EventsHelper struct {
}

func (instance *EventsHelper) On(eventName string, callback func(event *gg_events.Event)) *EventsHelper {
	if nil != instance && nil != Runtime {
		events := Runtime.Events()
		if nil != events {
			events.On(eventName, callback)
		}
	}
	return instance
}

func (instance *EventsHelper) Off(eventName string, callbacks ...func(event *gg_events.Event)) *EventsHelper {
	if nil != instance && nil != Runtime {
		events := Runtime.Events()
		if nil != events {
			events.Off(eventName, callbacks...)
		}

	}
	return instance
}

func (instance *EventsHelper) Emit(eventName string, args ...interface{}) *EventsHelper {
	if nil != instance && nil != Runtime {
		events := Runtime.Events()
		if nil != events {
			events.Emit(eventName, args...)
		}
	}
	return instance
}

func (instance *EventsHelper) EmitLogEvent(item *ggxcommons.LogMessage) *EventsHelper {
	if nil != instance {
		instance.Emit(ggxcommons.EventOnLog, item)
	}
	return instance
}

func (instance *EventsHelper) EmitStatEvent(item *ggxcommons.StatMessage) *EventsHelper {
	if nil != instance {
		instance.Emit(ggxcommons.EventOnStat, item)
	}
	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	d e l e g a t o r
// ---------------------------------------------------------------------------------------------------------------------

var Delegator *DelegatorHelper

type DelegatorHelper struct {
}

func (instance *DelegatorHelper) Register(funcName string, f gg_promise.FnSolve) {
	if nil != instance && nil != Runtime {
		delegator := Runtime.Delegator()
		if nil != delegator {
			delegator.Register(funcName, f)
		}
	}
}

func (instance *DelegatorHelper) Run(funcName string, args ...interface{}) (response interface{}, err error) {
	if nil != instance && nil != Runtime {
		delegator := Runtime.Delegator()
		if nil != delegator {
			response, err = delegator.Run(funcName, args...)
		}
	}
	return
}

func (instance *DelegatorHelper) RunTimed(funcName string, execTimeout time.Duration, args ...interface{}) (response interface{}, err error) {
	if nil != instance && nil != Runtime {
		delegator := Runtime.Delegator()
		if nil != delegator {
			response, err = delegator.RunTimed(funcName, execTimeout, args...)
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func SetAppName(value string) {
	gg.AppName = value
}

func GetAppName() string {
	return gg.AppName
}

func SetAppVersion(value string) {
	gg.AppVersion = value
}

func GetAppVersion() string {
	return gg.AppVersion
}

func SetAppSalt(value string) {
	ggxcommons.Salt = value
}

func GetAppSalt() string {
	return ggxcommons.Salt
}

func GetAppMode() string {
	if nil != Runtime {
		return Runtime.Mode()
	}
	return ""
}

func IsDebugMode() bool {
	if nil != Runtime {
		return Runtime.Mode() == ModeDebug
	}
	return false
}

func GetAppDirWork() string {
	if nil != Runtime {
		return Runtime.DirWork()
	}
	return ""
}

// ReadConfigFile read a configuration file by name or by pattern i.e. "app-settings.%s.json"
func ReadConfigFile(patternOrName string) (string, error) {
	if nil != Runtime {
		filename := fmt.Sprintf(patternOrName, Runtime.Mode())
		filename = gg.Paths.Absolutize(filename, Runtime.DirWork())
		return gg.IO.ReadTextFromFile(filename)
	}
	return "", nil
}

// GetAppLogger
// Returns the application logger
func GetAppLogger() gg_log.ILogger {
	if nil != Runtime {
		return Runtime.Logger()
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	utils
// ---------------------------------------------------------------------------------------------------------------------

// Recover error recover function
// pass arguments like logger and/or context message
func Recover(args ...interface{}) {
	gg.Recover(args...)
}

func MD5(value string) string {
	return gg.Coding.MD5(strings.ToLower(value))
}

func Encrypt(value string) string {
	return ggxcommons.Encrypt(value)
}

func Decrypt(value string) string {
	return ggxcommons.Decrypt(value)
}

func Slugify(value string) string {
	value = strings.ToLower(gg.Strings.Slugify(value, "_"))
	value = strings.ReplaceAll(value, "&", "_")
	value = strings.ReplaceAll(value, ".", "")
	return value
}

// ---------------------------------------------------------------------------------------------------------------------
//	tokens
// ---------------------------------------------------------------------------------------------------------------------

func TokenSetKey(newSignature string) {
	ggxcommons.TokenSetKey(newSignature)
}

func TokenCreate(uid interface{}, payload map[string]interface{}, duration time.Duration) (signed string, err error) {
	return ggxcommons.TokenCreate(uid, payload, duration)
}

func TokenValidate(stringToken string) (bool, error) {
	return ggxcommons.TokenValidate(stringToken)
}

func TokenPayload(stringToken string) (payload map[string]interface{}, err error) {
	return ggxcommons.TokenPayload(stringToken)
}

func TokenParse(stringToken string) (response map[string]interface{}, err error) {
	return ggxcommons.TokenParse(stringToken)
}

func TokenExpireDate(stringToken string) (expDate time.Time, err error) {
	return ggxcommons.TokenExpireDate(stringToken)
}

// ---------------------------------------------------------------------------------------------------------------------
//	errors
// ---------------------------------------------------------------------------------------------------------------------

func IsRecordNotFoundError(err error) bool {
	return nil != err && err.Error() == "record not found"
}

// ---------------------------------------------------------------------------------------------------------------------
//	webserver utils
// ---------------------------------------------------------------------------------------------------------------------

// WebAddFileExtensionToWhiteList
// Add a file extension to internal file pass-list
func WebAddFileExtensionToWhiteList(fileExt string) {
	if gg.Arrays.IndexOf(fileExt, ggxserve_webserver.FileWhitelist) == -1 {
		ggxserve_webserver.FileWhitelist = append(ggxserve_webserver.FileWhitelist, fileExt)
	}
}

// WebAuthorizeFile
// Authorize a file for upload or download using a white list
func WebAuthorizeFile(filename string) (success bool, err error) {
	return ggxserve_webserver.AuthorizeFile(filename)
}

// WebGetParams
// Retrieve all parameters in body or url
func WebGetParams(ctx *fiber.Ctx, flatData bool, selectOnly ...string) map[string]interface{} {
	return ggxserve_webserver.Params(ctx, flatData, selectOnly...)
}

func WebResponseWrite(ctx *fiber.Ctx, data interface{}, err error) error {
	return ggxserve_webserver.WriteResponse(ctx, data, err)
}

func WebResponseWriteHtml(ctx *fiber.Ctx, html string) error {
	return ggxserve_webserver.WriteHtml(ctx, html)
}

func WebResponseWriteDownload(ctx *fiber.Ctx, bytes []byte, contentType, filename string) error {
	return ggxserve_webserver.WriteDownload(ctx, bytes, contentType, filename)
}

func WebResponseWriteContent(ctx *fiber.Ctx, bytes []byte, contentType string) error {
	return ggxserve_webserver.WriteContentType(ctx, bytes, contentType)
}

func WebResponseWriteImage(ctx *fiber.Ctx, bytes []byte) error {
	return ggxserve_webserver.WriteImage(ctx, bytes)
}

func WebUpload(ctx *fiber.Ctx, root string, sizeLimit int64) ([]string, error) {
	return ggxserve_webserver.Upload(ctx, root, sizeLimit)
}

func WebUploadMultipart(ctx *fiber.Ctx, form *multipart.Form, root string, useDatePath bool, sizeLimit int64, fileSuffix string) ([]string, error) {
	return ggxserve_webserver.UploadMultipart(ctx, form, root, useDatePath, sizeLimit, fileSuffix)
}

func WebUploadGetWhitelistExt() []string {
	return ggxserve_webserver.FileWhitelist
}

func WebUploadGetBlacklistExt() []string {
	return ggxserve_webserver.FileBlacklist
}

func WebUploadAddToWhitelistExt(ext string) []string {
	if !strings.HasPrefix(ext, ".") {
		ext = "." + ext
	}
	if gg.Arrays.IndexOf(ext, ggxserve_webserver.FileWhitelist) == -1 {
		ggxserve_webserver.FileWhitelist = append(ggxserve_webserver.FileWhitelist, ext)
	}
	return ggxserve_webserver.FileWhitelist
}

func WebUploadAddToBlacklistExt(ext string) []string {
	if !strings.HasPrefix(ext, ".") {
		ext = "." + ext
	}
	if gg.Arrays.IndexOf(ext, ggxserve_webserver.FileBlacklist) == -1 {
		ggxserve_webserver.FileBlacklist = append(ggxserve_webserver.FileBlacklist, ext)
	}
	return ggxserve_webserver.FileBlacklist
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------
