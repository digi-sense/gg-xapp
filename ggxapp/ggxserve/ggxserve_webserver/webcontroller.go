package ggxserve_webserver

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httpserver/httprewrite"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxconfig"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcontroller"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcontroller/ggxcommand"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_templates"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/valyala/fasthttp"
	"strings"
)

type GGxWebController struct {
	root   string
	logger gg_log.ILogger
	events *gg_events.Emitter

	webserver        *Webserver
	websecure        ggxcommons.IWebsecure
	templates        *ggxserve_templates.GGxTemplates
	commands         *ggxcommand.GGxCommands
	externalCommands map[string]string
}

func NewWebController(mode string, logger gg_log.ILogger, events *gg_events.Emitter, commands *ggxcommand.GGxCommands) (instance *GGxWebController) {
	root := gg.Paths.WorkspacePath("./webserver")
	_ = gg.Paths.Mkdir(root + gg_utils.OS_PATH_SEPARATOR)

	instance = new(GGxWebController)
	instance.logger = logger
	instance.events = events
	instance.commands = commands
	instance.root = root
	instance.externalCommands = make(map[string]string)

	instance.init(mode, root)

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxWebController) SetTemplateEngine(templates *ggxserve_templates.GGxTemplates) *GGxWebController {
	if nil != instance {
		instance.templates = templates
	}
	return instance
}

func (instance *GGxWebController) Start() bool {
	if nil != instance {
		instance.webserver.Start()
		return true
	}
	return false
}

func (instance *GGxWebController) Stop() {
	if nil != instance {
		instance.webserver.Stop()
	}
}

func (instance *GGxWebController) UseRewrite(rules []*httprewrite.Rule) {
	if nil != instance && nil != instance.webserver {
		instance.Use(httprewrite.New(rules))
	}
}

func (instance *GGxWebController) Use(middleware fiber.Handler) {
	if nil != instance && nil != instance.webserver {
		instance.webserver.Use(middleware)
	}
}

// Handle expose handle method to add more
func (instance *GGxWebController) Handle(method, endpoint string, handler fiber.Handler) {
	if nil != instance && nil != instance.webserver {
		instance.webserver.Handle(method, endpoint, handler)
	}
}

// RegisterNoAuth register command with no http auth
func (instance *GGxWebController) RegisterNoAuth(method, endpoint, command string) {
	if nil != instance && nil != instance.webserver {
		uid := strings.ToLower(fmt.Sprintf("no-auth|%s|%s", method, GetPathNoParams(endpoint)))
		instance.externalCommands[uid] = command
		instance.webserver.Handle(method, endpoint, instance.onNoAuthHandler)
	}
}

// RegisterAuth register command that require http auth
func (instance *GGxWebController) RegisterAuth(method, endpoint, command string) {
	if nil != instance && nil != instance.webserver {
		uid := strings.ToLower(fmt.Sprintf("auth|%s|%s", method, GetPathNoParams(endpoint)))
		instance.externalCommands[uid] = command
		instance.webserver.Handle(method, endpoint, instance.onAuthHandler)
	}
}

func (instance *GGxWebController) GetWWWRoot() string {
	if nil != instance {
		return instance.root
	}
	return ""
}

func (instance *GGxWebController) GetApiUrl() string {
	if nil != instance {
		settings := instance.SettingsApi()
		if nil != settings {
			return settings.ApiBaseUrl
		}
	}
	return ""
}

func (instance *GGxWebController) FmtApiUrl(relative string, params map[string]interface{}) string {
	if nil != instance {
		base := instance.GetApiUrl()
		return fmtUrl(base, relative, params)
	}
	return ""
}

func (instance *GGxWebController) GetAppUrl() string {
	if nil != instance {
		settings := instance.SettingsApi()
		if nil != settings {
			return settings.AppBaseUrl
		}
	}
	return ""
}

func (instance *GGxWebController) FmtAppUrl(relative string, params map[string]interface{}) string {
	if nil != instance {
		base := instance.GetAppUrl()
		return fmtUrl(base, relative, params)
	}
	return ""
}

func (instance *GGxWebController) Settings() *ggxconfig.WebserverSettings {
	if nil != instance && nil != instance.webserver {
		return instance.webserver.settings
	}
	return nil
}

func (instance *GGxWebController) SettingsApi() *ggxconfig.AuthApi {
	if nil != instance && nil != instance.webserver {
		return instance.webserver.settings.Api
	}
	return nil
}

func (instance *GGxWebController) SettingsAuthorization() *ggxconfig.Authorization {
	if nil != instance && nil != instance.webserver && nil != instance.webserver.settings {
		return instance.webserver.settings.Auth
	}
	return nil
}

func (instance *GGxWebController) GetSettingItem(path string) (response interface{}) {
	if nil != instance {
		settings := instance.Settings()
		if nil != settings {
			return gg.Reflect.Get(settings.ToMap(), path)
		}
	}
	return
}

func (instance *GGxWebController) HttpAuth() *ggxconfig.Authorization {
	if nil != instance && nil != instance.webserver {
		return instance.webserver.HttpAuth()
	}
	return nil
}

func (instance *GGxWebController) SetWebSecure(websecure ggxcommons.IWebsecure) {
	if nil != instance {
		instance.websecure = websecure
	}
}

func (instance *GGxWebController) AuthenticateRequest(ctx *fiber.Ctx, assert bool) bool {
	if nil != instance && nil != instance.websecure {
		auth := instance.SettingsAuthorization()
		return instance.websecure.AuthenticateRequest(ctx, assert, auth)
	}
	return true
}

func (instance *GGxWebController) GetAuthId(ctx *fiber.Ctx) string {
	if nil != instance && nil != instance.websecure {
		return instance.websecure.GetAuthId(ctx)
	}
	return ""
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxWebController) init(mode, root string) {
	if nil != instance {
		instance.webserver = NewWebserver("webserver", mode, root, instance.logger)
		if nil != instance.webserver {

			// API SYS
			instance.webserver.Handle("get", ggxcommons.ApiSysVersion, instance.onSysVersion)
			instance.webserver.Handle("post", ggxcommons.ApiSysQrCode, instance.onSysQrCode)
		}

		// web controller initialized
		// all submodules API should be already declared
		instance.logger.Debug(fmt.Sprintf("INIT '%s' v%s: finished to initialize internal web controller.",
			gg.AppName, gg.AppVersion))
	}
}

func (instance *GGxWebController) onNoAuthHandler(ctx *fiber.Ctx) error {
	// PANIC RECOVERY
	defer ggxcommons.Recover("webcontroller.onNoAuthHandler")

	method := strings.ToLower(ctx.Method())
	uri := ctx.Request().URI()
	if len(method) > 0 && nil != uri {
		// endpoint := uri.Path()
		uid := getCommandUidFromURI("no-auth", method, uri)
		if command, ok := getCommand(instance.externalCommands, uid); ok {
			if len(command) > 0 {
				params := Params(ctx, true)
				userUid := gg.Reflect.GetString(params, "sender")
				response := instance.commands.Execute(ggxcontroller.RoleApiWeb, userUid, command, params, nil)
				if response.Response.IsError() {
					return WriteResponse(ctx, nil, response.Response.GetError())
				} else {
					return WriteResponse(ctx, response.Response.Result, nil)
				}
			}
		}
	}
	return nil
}

func (instance *GGxWebController) onAuthHandler(ctx *fiber.Ctx) error {
	// PANIC RECOVERY
	defer ggxcommons.Recover("webcontroller.onAuthHandler")

	method := strings.ToLower(ctx.Method())
	uri := ctx.Request().URI()
	if len(method) > 0 && nil != uri {
		uid := getCommandUidFromURI("auth", method, uri)
		if command, ok := getCommand(instance.externalCommands, uid); ok {
			if len(command) > 0 {
				if instance.AuthenticateRequest(ctx, true) {
					params := Params(ctx, true)
					authId := instance.GetAuthId(ctx)
					if len(authId) == 0 {
						authId = gg.Reflect.GetString(params, "sender")
					}
					response := instance.commands.Execute(ggxcontroller.RoleApiWeb, authId, command, params, nil)
					if response.Response.IsError() {
						return WriteResponse(ctx, nil, response.Response.GetError())
					} else {
						return WriteResponse(ctx, response.Response.Result, nil)
					}
				}
			}
		}
	}
	return nil
}

/** SYS **/
func (instance *GGxWebController) onSysVersion(ctx *fiber.Ctx) error {
	// PANIC RECOVERY
	defer ggxcommons.Recover("webcontroller.onSysVersion")

	// no auth
	params := Params(ctx, true)
	userUid := gg.Reflect.GetString(params, "sender")
	response := instance.commands.Execute(ggxcontroller.RoleApiWeb, userUid, ggxcommand.CmdSysVersion, params, nil)
	if response.Response.IsError() {
		return WriteResponse(ctx, nil, response.Response.GetError())
	} else {
		return WriteResponse(ctx, response.Response.Result, nil)
	}
}

func (instance *GGxWebController) onSysQrCode(ctx *fiber.Ctx) error {
	// PANIC RECOVERY
	defer ggxcommons.Recover("webcontroller.onSysQrCode")

	// no auth
	params := Params(ctx, true)
	userUid := gg.Reflect.GetString(params, "sender")
	response := instance.commands.Execute(ggxcontroller.RoleApiWeb, userUid, ggxcommand.CmdSysQrCode, params, nil)
	if response.Response.IsError() {
		return WriteResponse(ctx, nil, response.Response.GetError())
	} else {
		raw := response.Response.Result
		if bytes, ok := raw.([]byte); ok {
			return WriteImage(ctx, bytes) // Response(ctx, response.Response.Result, nil)
		} else {
			return WriteResponse(ctx, nil, gg.Errors.Prefix(ggxcommons.ExecutionError, "oSysQrCode expected an image as response!"))
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C   p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func fmtUrl(base, relative string, params map[string]interface{}) string {
	if len(base) > 0 {
		uri := gg.Paths.Concat(base, relative)
		if len(params) > 0 {
			count := 0
			for k, v := range params {
				if count > 0 {
					uri += "&"
				}
				uri += fmt.Sprintf("%s=%v", k, v)
				count++
			}
		}
		return uri
	}
	return ""
}

func getCommandUidFromURI(prefix, method string, uri *fasthttp.URI) (response string) {
	endpoint := GetPathNoParamsByUri(uri)
	response = strings.ToLower(fmt.Sprintf("%s|%s|%s", prefix, method, endpoint))
	return
}

func getCommand(registered map[string]string, uid string) (string, bool) {
	if command, ok := registered[uid]; ok {
		return command, true
	}
	commands := make([]string, 0)
	for k, command := range registered {
		if strings.Index(uid, k) == 0 {
			commands = append(commands, command)
		}
	}
	if len(commands) == 1 {
		return commands[0], true
	}
	return "", false
}
