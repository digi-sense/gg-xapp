package ggxserve_webserver

import "bitbucket.org/digi-sense/gg-core"

type WebFile struct {
	Bytes    []byte
	FullPath string
	FileName string
	FileExt  string
	MimeType string
}

func NewWebFile(fullPath string, bytes []byte) (instance *WebFile) {
	instance = new(WebFile)
	instance.FullPath = fullPath
	instance.Bytes = bytes
	instance.refresh()
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *WebFile) refresh() {
	if nil != instance {
		if len(instance.FullPath) > 0 {
			instance.FileName = gg.Paths.FileName(instance.FullPath, false)
			instance.FileExt = gg.Paths.Extension(instance.FullPath)
			instance.MimeType = gg.MIME.GetMimeTypeByExtension(instance.FileExt)
		}
	}
}
