package ggxserve_webserver

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httpserver"
	"bitbucket.org/digi-sense/gg-core-x/gg_http/httpserver/httprewrite"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxconfig"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"os"
	"strings"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type Webserver struct {
	name        string
	mode        string
	dirWork     string // workspace
	logger      gg_log.ILogger
	initialized bool
	enabled     bool

	settings       *ggxconfig.WebserverSettings
	httpserver     *httpserver.HttpServer
	httpRoot       string
	httpStaticRoot string
	httpAddr       string
	httpsAddr      string
}

func NewWebserver(name, mode string, httpRoot string, logger gg_log.ILogger) *Webserver {
	instance := new(Webserver)
	instance.name = name
	instance.mode = mode
	instance.logger = logger
	instance.dirWork = gg.Paths.WorkspacePath("./")
	instance.httpRoot = httpRoot
	instance.enabled = false

	_ = instance.init(name, mode)

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Webserver) HttpRoot() string {
	if nil != instance {
		return instance.httpRoot
	}
	return ""
}

func (instance *Webserver) HttpPath(path string) string {
	if nil != instance {
		return gg.Paths.Concat(instance.httpRoot, path)
	}
	return ""
}

func (instance *Webserver) HttpStaticRoot() string {
	if nil != instance {
		return instance.httpStaticRoot
	}
	return ""
}

func (instance *Webserver) HttpAddress() string {
	if nil != instance {
		return instance.httpAddr
	}
	return ""
}

func (instance *Webserver) HttpAuth() *ggxconfig.Authorization {
	if nil != instance && nil != instance.settings {
		return instance.settings.Auth
	}
	return nil
}

func (instance *Webserver) LocalUrl() string {
	if nil != instance && instance.enabled {
		if len(instance.httpAddr) > 0 {
			return fmt.Sprintf("http://localhost%v/", instance.httpAddr)
		} else if len(instance.httpsAddr) > 0 {
			return fmt.Sprintf("https://localhost%v/", instance.httpsAddr)
		}
	}
	return ""
}

func (instance *Webserver) IsHttps() bool {
	return len(instance.httpsAddr) > 0
}

func (instance *Webserver) Settings() *ggxconfig.WebserverSettings {
	if nil != instance {
		return instance.settings
	}
	return nil
}

func (instance *Webserver) Handle(method, endpoint string, handler fiber.Handler) {
	// PANIC RECOVERY
	defer ggxcommons.Recover("webserver.Handle")

	switch strings.ToLower(method) {
	case "get":
		instance.httpserver.Get(endpoint, handler)
	case "post":
		instance.httpserver.Post(endpoint, handler)
	case "put":
		instance.httpserver.Put(endpoint, handler)
	case "delete":
		instance.httpserver.Delete(endpoint, handler)
	case "middleware":
		instance.httpserver.Middleware(endpoint, handler)
	default:
		instance.httpserver.All(endpoint, handler)
	}
}

func (instance *Webserver) UseRewrite(rules []*httprewrite.Rule) {
	if nil != instance && nil != instance.httpserver {
		instance.Use(httprewrite.New(rules))
	}
}

func (instance *Webserver) Use(middleware fiber.Handler) {
	if nil != instance && nil != instance.httpserver {
		instance.httpserver.Use(middleware)
	}
}

func (instance *Webserver) IsEnabled() bool {
	if nil != instance {
		return instance.enabled
	}
	return false
}

func (instance *Webserver) Start() bool {
	if nil != instance && instance.enabled {
		instance.start()

		return true
	}
	return false
}

func (instance *Webserver) Stop() {
	if nil != instance && instance.enabled {
		_ = instance.httpserver.Stop()
	}
}

func (instance *Webserver) Exit() {
	defer func() {
		if r := recover(); r != nil {
			// recover from panic if any
		}
	}()
	if nil != instance && instance.enabled {
		go func() {
			_ = instance.httpserver.Stop()
		}()
		// wait a while the server close
		time.Sleep(3 * time.Second)
		if instance.httpserver.IsOpen() {
			// brute force close
			os.Exit(0)
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Webserver) init(name, mode string) (err error) {
	if !instance.initialized {
		instance.initialized = true

		// search for configuration file
		var settings *ggxconfig.WebserverSettings
		settings, err = loadSettings(name, mode)
		if nil == err {
			instance.settings = settings
			instance.enabled = settings.Enabled
			if instance.enabled {

				// create webserver
				err = instance.initHttp()
				if nil != err {
					instance.enabled = false
					return err
				}
			}
		} else {
			instance.enabled = false
		}
	}
	return
}

func (instance *Webserver) initHttp() error {
	// create webserver instance
	httpConfig := instance.settings.Http

	instance.httpserver = httpserver.NewHttpServer(instance.httpRoot, instance.handleHttpError, instance.handleHttpLimit)
	err := instance.httpserver.ConfigureFromMap(httpConfig)
	if nil != err {
		return err
	}
	// check defaults
	configuration := instance.httpserver.Configuration()
	if nil != configuration {
		if nil != configuration.Server {
			if configuration.Server.ReadTimeout < 3*time.Second {
				configuration.Server.ReadTimeout = 3 * time.Second
			}
			if configuration.Server.WriteTimeout < 3*time.Second {
				configuration.Server.WriteTimeout = 3 * time.Second
			}
		}
	}

	// parse settings
	hosts := gg.Reflect.GetArray(httpConfig, "hosts")
	for _, host := range hosts {
		isSecure := gg.Reflect.GetBool(host, "tls")
		if isSecure {
			instance.httpsAddr = gg.Reflect.GetString(host, "addr")
			sslCert := gg.Reflect.GetString(host, "ssl_cert")
			sslKey := gg.Reflect.GetString(host, "ssl_key")
			if len(sslKey) > 0 {
				instance.mkDir(sslKey)
			}
			if len(sslCert) > 0 {
				instance.mkDir(sslCert)
			}
		} else {
			instance.httpAddr = gg.Reflect.GetString(host, "addr")
		}
	}
	static := gg.Reflect.GetArray(httpConfig, "static")
	if len(static) > 0 {
		root := gg.Reflect.GetString(static[0], "root")
		instance.httpStaticRoot = gg.Paths.Concat(instance.httpRoot, root)
		instance.mkDir(instance.httpStaticRoot + gg_utils.OS_PATH_SEPARATOR)
	}

	return nil
}

func (instance *Webserver) start() {
	// wait a while before start to allow runtime is ready
	time.Sleep(2 * time.Second)
	instance.httpserver.Start()
}

func (instance *Webserver) handleHttpError(serverError *httpserver.HttpServerError) {
	if nil != instance && nil != instance.logger {
		instance.logger.Error("webserver.handleHttpError()", serverError.Error)
	}
}

func (instance *Webserver) handleHttpLimit(ctx *fiber.Ctx) error {
	if nil != instance && nil != instance.logger {
		instance.logger.Error("webserver.handleHttpLimit()")
	}
	return nil
}

func (instance *Webserver) mkDir(path string) {
	if gg.Paths.IsAbs(path) {
		_ = gg.Paths.Mkdir(path)
	} else {
		_ = gg.Paths.Mkdir(gg.Paths.Concat(instance.httpRoot, path))
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func loadSettings(name, mode string) (*ggxconfig.WebserverSettings, error) {
	path := gg.Paths.WorkspacePath(name + "." + mode + ".json")
	settings := new(ggxconfig.WebserverSettings)
	text, err := gg.IO.ReadTextFromFile(path)
	if nil != err {
		return settings, err
	}
	err = gg.JSON.Read(text, &settings)
	return settings, err
}
