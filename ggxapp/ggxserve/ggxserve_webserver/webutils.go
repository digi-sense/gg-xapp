package ggxserve_webserver

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"errors"
	"fmt"
	"github.com/gofiber/fiber/v2"
	"github.com/valyala/fasthttp"
	"mime/multipart"
	"net/url"
	"strconv"
	"strings"
)

var FileWhitelist = []string{
	".png", ".jpg", ".jpeg", ".gif", ".ico", ".tiff", 
	".doc", ".docx", ".ppt", ".pptx", ".xls", ".xlsx",
	".mov", ".wmv", ".mpg", ".mp2", ".mpeg", ".mpv", ".mpe",
	".pdf",
}

var FileBlacklist = []string{
	".bat", ".sh", ".exe",
}

var ErrorUnauthorizedResource = errors.New("Unauthorized resource.")

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func GetPathNoParamsByUri(uri *fasthttp.URI) (response string) {
	response = GetPathNoParams(string(uri.Path()))
	return
}

func GetPathNoParams(path string) (response string) {
	response = path
	idx := strings.Index(response, "/:")
	if idx > 5 {
		response = response[:idx]
	}
	return
}

func AuthorizeFile(filename string) (success bool, err error) {
	success = false
	if strings.Index(filename, "..") > -1 || strings.Index(filename, "../") > -1 || strings.Index(filename, "..\\") > -1 {
		err = ErrorUnauthorizedResource
		return
	}
	ext := strings.ToLower(gg.Paths.Extension(filename))

	// check blacklist
	for _, b := range FileBlacklist {
		fail := b == ext
		if fail {
			err = ErrorUnauthorizedResource
			return
		}
	}

	// check whitelist
	for _, w := range FileWhitelist {
		success = (w == ext) || w == ".*"
		if success {
			break
		}
	}

	if !success {
		err = ErrorUnauthorizedResource
	}
	return
}

func WriteContentType(ctx *fiber.Ctx, bytes []byte, contentType string) error {
	if len(contentType) == 0 {
		contentType = "application/octet-stream"
	}
	ctx.Response().Header.Set("Content-Type", contentType)
	ctx.Response().SetBody(bytes)
	return nil
}

func WriteFile(ctx *fiber.Ctx, webFile *WebFile) error {
	if nil != webFile {
		if len(webFile.MimeType) == 0 {
			webFile.MimeType = "application/octet-stream"
		}
		return WriteDownload(ctx, webFile.Bytes, webFile.MimeType, webFile.FileName)
	}
	return nil
}

func WriteDownload(ctx *fiber.Ctx, bytes []byte, contentType, filename string) error {
	if len(contentType) == 0 {
		contentType = "application/octet-stream"
	}
	ctx.Response().Header.Set("Content-Type", contentType)
	ctx.Response().Header.Set("Content-Disposition", fmt.Sprintf("filename=\"%s\"", filename))
	ctx.Response().SetBody(bytes)
	return nil
}

func WriteHtml(ctx *fiber.Ctx, html string) error {
	ctx.Response().Header.Set("Content-Type", "text/html")
	ctx.Response().SetBody([]byte(html))
	return nil
}

func WriteImage(ctx *fiber.Ctx, bytes []byte) error {
	ctx.Response().Header.Set("Content-Type", "image/jpeg")
	ctx.Response().Header.Set("Content-Length", strconv.Itoa(len(bytes)))
	ctx.Response().SetBody(bytes)
	return nil
}

func WriteResponse(ctx *fiber.Ctx, data interface{}, err error) error {
	m := map[string]interface{}{}

	// error handler
	if nil != err {
		errMessage := err.Error()
		m["error"] = errMessage
		if errMessage == ggxcommons.HttpUnauthorizedError.Error() || errMessage == ggxcommons.HttpInvalidCredentialsError.Error() {
			ctx.Response().SetStatusCode(401)
			if nil == data {
				data = map[string]interface{}{"code": 401, "message": ggxcommons.HttpUnauthorizedError.Error()}
			}
		} else if errMessage == ggxcommons.AccessTokenExpiredError.Error() {
			ctx.Response().SetStatusCode(403)
			if nil == data {
				data = map[string]interface{}{"code": 403, "message": ggxcommons.AccessTokenExpiredError.Error()}
			}
		} else if errMessage == ggxcommons.RefreshTokenExpiredError.Error() {
			ctx.Response().SetStatusCode(401)
			if nil == data {
				data = map[string]interface{}{"code": 401, "message": ggxcommons.RefreshTokenExpiredError.Error()}
			}
		} else if errMessage == ggxcommons.AccessTokenInvalidError.Error() {
			ctx.Response().SetStatusCode(401)
			if nil == data {
				data = map[string]interface{}{"code": 401, "message": ggxcommons.AccessTokenInvalidError.Error()}
			}
		} else {
			ctx.Response().SetStatusCode(500)
			if nil == data {
				data = map[string]interface{}{"code": 500, "message": err.Error()}
			}
		}
	}

	// write the response
	if nil != data {
		if ok, _ := gg.Compare.IsMap(data); ok {
			// MAP
			m["response"] = toJson(data)
			ctx.Response().Header.Set("Content-Type", "text/json")
			ctx.Response().SetBody([]byte(gg.JSON.Stringify(m)))
			return nil
		} else if ok, _ := gg.Compare.IsString(data); ok {
			// STRING
			if gg.Regex.IsHTML(data.(string)) {
				return WriteHtml(ctx, data.(string))
			} else {
				// JSON fallback
				m["response"] = toJson(data)
				ctx.Response().Header.Set("Content-Type", "text/json")
				ctx.Response().SetBody([]byte(gg.JSON.Stringify(m)))
				return nil
			}
		} else if ok, _ := gg.Compare.IsArray(data); ok {
			// ARRAY
			if bytes, ok := data.([]byte); ok {
				return WriteImage(ctx, bytes)
			} else {
				m["response"] = toJson(data)
				ctx.Response().Header.Set("Content-Type", "text/json")
				ctx.Response().SetBody([]byte(gg.JSON.Stringify(m)))
				return nil
			}
		} else if wf, ok := data.(*WebFile); ok {
			return WriteFile(ctx, wf)
		} else if wf, ok := data.(WebFile); ok {
			return WriteFile(ctx, &wf)
		}
	}

	// empty response
	m["response"] = "EMPTY_RESPONSE"
	ctx.Response().Header.Set("Content-Type", "text/json")
	ctx.Response().SetBody([]byte(gg.JSON.Stringify(m)))
	return nil
}

func BodyMap(ctx *fiber.Ctx, flatData bool) map[string]interface{} {
	var m map[string]interface{}
	body := ctx.Body()
	_ = gg.JSON.Read(body, &m)

	if data, b := m["data"].(map[string]interface{}); b && flatData {
		delete(m, "data")
		for k, v := range data {
			m[k] = v
		}
	}

	return m
}

func Params(ctx *fiber.Ctx, flatData bool, selectOnly ...string) map[string]interface{} {
	// get all body params
	response := BodyMap(ctx, flatData)
	if nil == response {
		response = make(map[string]interface{})
	}

	// add context
	response["_ctx"] = ctx

	// try add form params
	if form, err := ctx.MultipartForm(); nil == err && nil != form {
		if nil != form.Value {
			for k, v := range form.Value {
				if _, b := response[k]; !b {
					if len(v) == 1 {
						response[k] = v[0]
					} else {
						response[k] = v
					}
				}
			}
		}
		// add form
		response["_form"] = form
	}

	// route
	routeParams := ctx.Route().Params
	if len(routeParams) > 0 {
		for _, rp := range routeParams {
			response[rp] = ctx.Params(rp)
		}
	}

	// url query
	if path := ctx.OriginalURL(); len(path) > 0 {
		uri, err := url.Parse(path)
		if nil == err {
			query := uri.Query()
			if nil != query && len(query) > 0 {
				for k, v := range query {
					if len(v) == 1 {
						response[k] = v[0]
					} else {
						response[k] = v
					}
				}
			}
		}
	}

	// evaluate filter
	if len(selectOnly) > 0 {
		m := map[string]interface{}{}
		for _, name := range selectOnly {
			if v, b := response[name]; b {
				m[name] = v
			}
		}
		return m
	}

	return response
}

func AssertParams(ctx *fiber.Ctx, names []string, selectOnly ...string) (map[string]interface{}, error) {
	params := Params(ctx, true, selectOnly...)
	if len(names) > 0 {
		missing := make([]string, 0)
		// get missing parameters
		for _, name := range names {
			if value, b := params[name]; !b || len(gg.Convert.ToString(value)) == 0 {
				missing = append(missing, name)
			}
		}
		if len(missing) > 0 {
			return nil, errors.New(fmt.Sprintf("missing_params:%v", strings.Join(missing, ",")))
		}
	}
	return params, nil
}

func MultipartForm(ctx *fiber.Ctx) (form *multipart.Form, err error) {
	form, err = ctx.MultipartForm()
	return
}

func Upload(ctx *fiber.Ctx, root string, sizeLimit int64) ([]string, error) {
	// get form
	form, err := ctx.MultipartForm()
	if nil != err {
		return nil, err // not a form request
	}

	return UploadMultipart(ctx, form, root, true, sizeLimit, gg.Coding.MD5(gg.Rnd.Uuid()))
}

func UploadMultipart(ctx *fiber.Ctx, form *multipart.Form, root string, useDatePath bool, sizeLimit int64, fileSuffix string) ([]string, error) {
	root = gg.Paths.Absolute(root) // absolute

	// removes any temporary files associated with the Form.
	defer form.RemoveAll()

	response := make([]string, 0)
	// loop on files
	for _, files := range form.File {
		// Loop through files:
		for _, file := range files {
			size := file.Size
			if sizeLimit > 0 && size > sizeLimit {
				continue
			}
			filename := file.Filename
			if _, e := AuthorizeFile(filename); nil != e {
				return []string{}, e
			}
			ext := gg.Paths.Extension(filename)
			name := gg.Paths.FileName(filename, false)
			dir := gg.Paths.Dir(filename)
			if len(fileSuffix) > 0 {
				filename = gg.Paths.Concat(dir, name+"_"+fileSuffix+ext)
			} else {
				filename = gg.Paths.Concat(dir, name+ext)
			}
			var path string
			if useDatePath {
				path = gg.Paths.DatePath(root, filename, 3, true)
			} else {
				path = gg.Paths.Concat(root, filename)
				_ = gg.Paths.Mkdir(path)
			}

			// save file
			err := ctx.SaveFile(file, path)
			if nil != err {
				return response, err
			}
			response = append(response, strings.Replace(path, root, ".", 1)) // relative path
		}
	}
	return response, nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func toJson(data interface{}) interface{} {
	if nil != data {
		return gg.JSON.Parse(gg.Convert.ToString(data))
	}
	return nil
}
