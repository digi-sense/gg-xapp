package query

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database/model"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database_pool"
	"fmt"
	"gorm.io/gorm"
)

// ---------------------------------------------------------------------------------------------------------------------
//	Log
// ---------------------------------------------------------------------------------------------------------------------

const NameQueryLog = "log"     // name of controller
const DbLog = ggxcommons.DbLog // settings name

var logInitialized bool

type LogQuery struct {
	pool *ggxserve_database_pool.Connections
}

func NewLogQuery(pool *ggxserve_database_pool.Connections) (response interface{}, err error) {
	instance := new(LogQuery)
	instance.pool = pool
	err = instance.init()
	response = instance

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LogQuery) Info(message, payload string) (err error) {
	if nil != instance {
		err = instance.Log(0, gg_log.InfoLevel, message, payload, "")
	}
	return
}

func (instance *LogQuery) Error(message, payload string) (err error) {
	if nil != instance {
		err = instance.Log(0, gg_log.ErrorLevel, message, payload, "")
	}
	return
}

func (instance *LogQuery) Warn(message, payload string) (err error) {
	if nil != instance {
		err = instance.Log(0, gg_log.WarnLevel, message, payload, "")
	}
	return
}

func (instance *LogQuery) Debug(message, payload string) (err error) {
	if nil != instance {
		err = instance.Log(0, gg_log.DebugLevel, message, payload, "")
	}
	return
}

func (instance *LogQuery) Log(userId uint, level gg_log.Level, message, payload, category string) (err error) {
	if nil != instance {

		item := new(model.Log)
		item.UserId = userId
		item.Level = level.String()
		item.Message = message
		item.Payload = payload
		item.Category = category

		err = instance.Save(item)
	}
	return
}

func (instance *LogQuery) LogCreate(userId uint, err error, level, message string, payload string) error {

	isError := nil != err
	if isError {
		if len(message) == 0 {
			message = err.Error()
		} else {
			message = fmt.Sprintf("%s - %s", message, err.Error())
		}
	}

	if len(level) == 0 {
		if isError {
			level = "error"
		} else {
			level = "info"
		}
	}

	item := &model.Log{
		UserId:  userId,
		Message: message,
		IsError: isError,
		Payload: payload,
		Level:   level,
	}

	return instance.Save(item)
}

func (instance *LogQuery) Save(item *model.Log) (err error) {
	if nil != instance && nil != item {

		db, e := instance.db()
		if nil != e {
			err = e
			return
		}

		tx := db.Create(item)
		if nil != tx.Error {
			err = gg.Errors.Prefix(tx.Error, "Saving item to collection 'log'")
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *LogQuery) init() error {
	// init schemas
	if !logInitialized {
		logInitialized = true

		db, e := instance.db()
		if nil != e {
			return e
		}

		err := db.AutoMigrate(&model.Log{})
		if err != nil {
			return gg.Errors.Prefix(err, "Error Migrating Schema 'Log'")
		}
	}
	return nil
}

func (instance *LogQuery) db() (db *gorm.DB, err error) {
	_, db, err = instance.pool.Get(DbLog)
	return
}
