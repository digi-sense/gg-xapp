package ggxserve_database

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database_pool"
)

type GGxDatabaseManager struct {
	mode     string
	root     string
	logger   gg_log.ILogger
	events   *gg_events.Emitter
	settings *ggxserve_database_pool.DbConfig // contains connections

	pool             *ggxserve_database_pool.Connections
	queryControllers map[string]QueryControllerBuilder
}

func NewDatabase(root, mode string, settings *ggxserve_database_pool.DbConfig, logger gg_log.ILogger, events *gg_events.Emitter) (*GGxDatabaseManager, error) {
	instance := new(GGxDatabaseManager)
	instance.mode = mode
	instance.settings = settings
	instance.logger = logger
	instance.events = events

	instance.root = root // gg.Paths.WorkspacePath("./database/")
	err := instance.init(mode)

	return instance, err
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxDatabaseManager) Root() (response string) {
	if nil != instance {
		response = instance.root
	}
	return
}

func (instance *GGxDatabaseManager) Path(relative string) (response string) {
	if nil != instance {
		response = gg.Paths.Concat(instance.root, relative)
	}
	return
}

func (instance *GGxDatabaseManager) IsEnabled() (response bool) {
	if nil != instance {
		response = nil != instance.settings && instance.settings.Enabled
	}
	return
}

func (instance *GGxDatabaseManager) Pool() *ggxserve_database_pool.Connections {
	if nil != instance && instance.IsEnabled() {
		return instance.pool
	}
	return nil
}

func (instance *GGxDatabaseManager) Connection(name string) *ggxserve_database_pool.Connection {
	if nil != instance && instance.IsEnabled() {
		return instance.pool.GetConnectionByName(name)
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//  query controllers
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxDatabaseManager) HasQueryController(uid string) bool {
	if nil != instance && instance.IsEnabled() {
		if _, ok := instance.queryControllers[uid]; ok {
			return true
		}
	}
	return false
}

func (instance *GGxDatabaseManager) GetQueryController(uid string) interface{} {
	if nil != instance && instance.IsEnabled() {
		if buildFunc, ok := instance.queryControllers[uid]; ok {
			item, err := buildFunc(instance.pool)
			if nil == err {
				return item
			}
		}
	}
	return nil
}

func (instance *GGxDatabaseManager) SetQueryController(uid string, buildFunc QueryControllerBuilder) {
	if nil != instance && instance.IsEnabled() {
		if _, ok := instance.queryControllers[uid]; !ok {
			instance.queryControllers[uid] = buildFunc
		}
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxDatabaseManager) init(mode string) (err error) {
	if instance.IsEnabled() {
		instance.queryControllers = make(map[string]QueryControllerBuilder)
		instance.pool = ggxserve_database_pool.NewPool(mode, instance.root, instance.settings)
	}

	return
}
