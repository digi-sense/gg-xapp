package ggxserve_database

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-core/gg_log"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database/query"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database_pool"
	"fmt"
	"gorm.io/gorm"
	"strings"
)

type GGxDatabaseController struct {
	mode   string
	root   string
	logger gg_log.ILogger
	events *gg_events.Emitter

	settings *ggxserve_database_pool.DbConfig // contains connections
	stopped  bool
	db       *GGxDatabaseManager
}

func NewDatabaseController(mode string, logger gg_log.ILogger, events *gg_events.Emitter) (*GGxDatabaseController, error) {
	instance := new(GGxDatabaseController)
	instance.mode = mode
	instance.logger = logger
	instance.events = events

	instance.root = gg.Paths.WorkspacePath("./database/")
	err := instance.init(mode)

	return instance, err
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxDatabaseController) Root() (response string) {
	if nil != instance {
		response = instance.root
	}
	return
}

func (instance *GGxDatabaseController) Path(relative string) (response string) {
	if nil != instance {
		response = gg.Paths.Concat(instance.root, relative)
	}
	return
}

func (instance *GGxDatabaseController) Close() {
	if nil != instance {
		// close everything
		instance.stopped = true
	}
}

func (instance *GGxDatabaseController) BuildDB(driver, dsn string) (db *gorm.DB, err error) {
	db, err = ggxserve_database_pool.Build(instance.root, driver, dsn, instance.mode)
	return
}

func (instance *GGxDatabaseController) CreateDB(driver, dsn, command string) (db *gorm.DB, err error) {
	db, err = ggxserve_database_pool.Build(instance.root, driver, dsn, instance.mode)
	if nil == err {
		if driver == "sqlite" {
			// SQLITE has no commands to execute
		} else {
			if len(command) > 0 {
				tx := db.Exec(command)
				if nil != tx.Error && strings.Index(tx.Error.Error(), "already exists") == -1 {
					err = tx.Error
					return
				}
			}
		}
	}
	return
}

func (instance *GGxDatabaseController) DropDB(driver, dsn, command string) (db *gorm.DB, err error) {
	db, err = ggxserve_database_pool.Build(instance.root, driver, dsn, instance.mode)
	if driver == "sqlite" {
		path := gg.Paths.Absolutize(dsn, instance.root)
		err = gg.IO.Remove(path)
	} else {
		if nil == err && len(command) > 0 {
			tx := db.Exec(command)
			if nil != tx.Error && strings.Index(tx.Error.Error(), "already exists") == -1 {
				err = tx.Error
				return
			}
		}
	}
	return
}

func (instance *GGxDatabaseController) Settings() *ggxserve_database_pool.DbConfig {
	if nil != instance && nil != instance.settings {
		return instance.settings
	}
	return nil
}

func (instance *GGxDatabaseController) Pool() *ggxserve_database_pool.Connections {
	if nil != instance && nil != instance.db && nil != instance.db.pool {
		return instance.db.pool
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//	c o n n e c t i o n s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxDatabaseController) ConnectionNames(tag string) []string {
	if nil != instance && nil != instance.db && nil != instance.db.pool {
		return instance.db.pool.ConnectionNames(tag)
	}
	return []string{}
}

func (instance *GGxDatabaseController) ResetConnections(connections []*ggxserve_database_pool.DbConfigConnection) {
	if nil != instance && nil != instance.db && nil != instance.db.pool {
		instance.db.pool.ResetConnections(connections)
	}
}

func (instance *GGxDatabaseController) Connection(name string) *ggxserve_database_pool.Connection {
	if nil != instance && nil != instance.db && nil != instance.db.pool {
		return instance.db.pool.GetConnectionByName(name)
	}
	return nil
}

func (instance *GGxDatabaseController) AddConnection(uid, driver, dsn string, saveToConfigFile bool) *GGxDatabaseController {
	if nil != instance && nil != instance.db.pool {
		instance.AddConnectionWithTag(uid, driver, dsn, "", saveToConfigFile)
	}
	return instance
}

func (instance *GGxDatabaseController) AddConnectionWithTag(uid, driver, dsn, tag string, saveToConfigFile bool) *GGxDatabaseController {
	if nil != instance && nil != instance.db.pool {
		conn := &ggxserve_database_pool.DbConfigConnection{
			Root:   instance.db.Root(),
			Uid:    uid,
			Driver: driver,
			Dsn:    dsn,
			Tag:    tag,
		}
		instance.AddConnectionFromConfig(conn, saveToConfigFile)
	}
	return instance
}

func (instance *GGxDatabaseController) AddConnectionFromJSON(json string, saveToConfigFile bool) *GGxDatabaseController {
	if nil != instance && nil != instance.db && nil != instance.db.pool {
		var conn *ggxserve_database_pool.DbConfigConnection
		err := gg.JSON.Read(json, &conn)
		if nil == err {
			_ = instance.AddConnectionFromConfig(conn, saveToConfigFile)
		}
	}
	return instance
}

func (instance *GGxDatabaseController) AddConnectionFromConfig(conn *ggxserve_database_pool.DbConfigConnection, saveToConfigFile bool) *GGxDatabaseController {
	if nil != instance && nil != instance.settings &&
		nil != instance.db && nil != instance.db.pool && nil != conn {
		// add missing root to connection
		if len(conn.Root) == 0 {
			conn.Root = instance.db.Root()
		}
		// check connection do not exist
		if !instance.settings.HasConnection(conn.Uid) {
			instance.settings.Connections = append(instance.settings.Connections, conn)
			instance.db.pool.AddConnection(conn)
			if saveToConfigFile {
				_ = instance.settings.Save()
			}
		}
	}
	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//  s c h e m a s
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxDatabaseController) AutoMigrateSchema(schemas map[string][]interface{}) (err error) {
	if nil != instance && nil != instance.db {
		for connName, models := range schemas {
			_, db, e := instance.db.pool.Get(connName)
			if nil != e {
				err = e
				break
			}
			err = db.AutoMigrate(models...)
			if nil != err {
				break
			}
		}
	}
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//  query controllers
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxDatabaseController) HasQueryController(uid string) bool {
	if nil != instance && nil != instance.db {
		return instance.db.HasQueryController(uid)
	}
	return false
}

func (instance *GGxDatabaseController) GetQueryController(uid string) interface{} {
	if nil != instance && nil != instance.db {
		return instance.db.GetQueryController(uid)
	}
	return nil
}

func (instance *GGxDatabaseController) SetQueryController(uid string, buildFunc QueryControllerBuilder) {
	if nil != instance && nil != instance.db {
		instance.db.SetQueryController(uid, buildFunc)
	}
}

func (instance *GGxDatabaseController) QueryLog() *query.LogQuery {
	if nil != instance {
		q := instance.GetQueryController("log")
		if nil != q {
			if item, ok := q.(*query.LogQuery); ok {
				return item
			}
		}
	}
	return nil
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxDatabaseController) init(mode string) (err error) {
	instance.stopped = false

	// settings
	filename := gg.Paths.WorkspacePath(fmt.Sprintf("database.%s.json", mode))
	instance.settings, err = ggxserve_database_pool.DbConfigLoadFromFile(filename)
	if nil != err {
		return
	}

	if nil != instance.settings && instance.settings.Enabled {

		// enable/disable encryption
		ggxcommons.Salt = instance.settings.GdprEnabledSalt

		instance.db, err = NewDatabase(instance.root, instance.mode, instance.settings, instance.logger, instance.events)

		// handle log events
		instance.events.On(ggxcommons.EventOnLog, instance.handleOnLogEvent)
	}

	return
}

func (instance *GGxDatabaseController) handleOnLogEvent(event *gg_events.Event) {
	controller := instance.QueryLog()
	if nil != controller {
		if item, ok := event.Argument(0).(*ggxcommons.LogMessage); ok {
			err := controller.LogCreate(item.UserId, item.Error, item.Level, item.Message, item.Payload)
			if nil != err {
				// unable to log to database
				instance.logger.Error(gg.Errors.Prefix(ggxcommons.PanicSystemError,
					fmt.Sprintf("Unable to write log into database: %s", err)))
			}
		}
	}
}
