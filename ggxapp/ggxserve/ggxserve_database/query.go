package ggxserve_database

import "bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_database_pool"

type QueryControllerBuilder func(pool *ggxserve_database_pool.Connections) (instance interface{}, err error)
