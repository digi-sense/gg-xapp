package model

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"gorm.io/gorm"
	"time"
)

type Log struct {
	ID         uint `gorm:"primarykey"`
	CreatedAt  time.Time
	UpdatedAt  time.Time
	Timestamp  int64
	DateTime   time.Time
	DateString string
	UserId     uint
	IsError    bool
	Message    string
	Payload    string
	Level      string
	Category   string
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Log) String() string {
	return gg.JSON.Stringify(instance)
}

func (instance *Log) BeforeSave(_ *gorm.DB) (err error) {
	instance.refresh()
	return
}

func (instance *Log) BeforeUpdate(_ *gorm.DB) (err error) {
	instance.refresh()
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Log) refresh() {
	if instance.Timestamp == 0 {
		instance.DateTime = time.Now()
		instance.Timestamp = instance.DateTime.Unix()
		instance.DateString = gg.Formatter.FormatDate(instance.DateTime, "yyyyMMdd HH:mm:ss Z")
	}
	if len(instance.Category) == 0 {
		if instance.UserId > 0 {
			instance.Category = ggxcommons.LogCategoryUser
		} else {
			instance.Category = ggxcommons.LogCategorySystem
		}
	}
}
