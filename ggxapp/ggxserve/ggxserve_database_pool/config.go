package ggxserve_database_pool

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
)

type DbConfig struct {
	Enabled         bool                  `json:"enabled"`
	GdprEnabledSalt string                `json:"gdpr-enabled-salt"`
	Connections     []*DbConfigConnection `json:"connections"`

	filename string
}

type DbConfigConnection struct {
	Root   string `json:"root"` // working dir
	Uid    string `json:"uid"`
	Driver string `json:"driver"`
	Dsn    string `json:"dsn"`
	Tag    string `json:"tag"`
}

func DbConfigLoadFromFile(filename string) (instance *DbConfig, err error) {
	instance = new(DbConfig)
	err = gg.JSON.ReadFromFile(filename, &instance)
	if nil == err {
		instance.filename = filename
	}
	return
}

func DbConfigSaveToFile(instance *DbConfig, filename string) (err error) {
	_, err = gg.IO.WriteTextToFile(instance.String(), filename)
	return
}

func (instance *DbConfig) String() string {
	if nil != instance {
		return gg.JSON.Stringify(instance)
	}
	return ""
}

func (instance *DbConfig) Save() error {
	if nil != instance && len(instance.filename) > 0 {
		return DbConfigSaveToFile(instance, instance.filename)
	}
	return gg.Errors.Prefix(ggxcommons.PanicSystemError, "Inconsistent file name: ")
}

func (instance *DbConfig) GetFilename() string {
	if nil != instance {
		return instance.filename
	}
	return ""
}

func (instance *DbConfig) SetFilename(filename string) *DbConfig {
	if nil != instance {
		instance.filename = filename
	}
	return instance
}

func (instance *DbConfig) RemoveConnection(uid string) *DbConfig {
	if nil != instance {
		connections := make([]*DbConfigConnection, 0)
		for _, conn := range instance.Connections {
			if conn.Uid != uid {
				connections = append(connections, conn)
			}
		}
	}
	return instance
}

func (instance *DbConfig) HasConnection(uid string) bool {
	if nil != instance {
		for _, conn := range instance.Connections {
			if conn.Uid == uid {
				return true
			}
		}
	}
	return false
}
