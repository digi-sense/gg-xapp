package ggxserve_database_pool

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"database/sql"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/driver/postgres"
	"gorm.io/driver/sqlite"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"strings"
	"sync"
)

type Connections struct {
	mode        string
	root        string // database root (used for file DB)
	settings    map[string]*DbConfigConnection
	connections map[string]*Connection
	mux         sync.Mutex
}

func NewPool(mode, root string, config *DbConfig) (instance *Connections) {
	instance = new(Connections)
	instance.mode = mode
	instance.root = root

	if nil != config {
		instance.ResetConnections(config.Connections)
	} else {
		instance.settings = make(map[string]*DbConfigConnection)
		instance.connections = make(map[string]*Connection)
	}

	return
}

func (instance *Connections) Reset() {
	_ = instance.CloseAll()
	instance.settings = make(map[string]*DbConfigConnection)
	instance.connections = make(map[string]*Connection)
}

func (instance *Connections) ResetConnections(connections []*DbConfigConnection) {
	instance.Reset()
	for _, dbSettings := range connections {
		instance.settings[dbSettings.Uid] = dbSettings
	}
}

func (instance *Connections) CloseAll() []error {
	errors := make([]error, 0)
	if len(instance.connections) > 0 {
		for _, conn := range instance.connections {
			err := conn.Close()
			if nil != err {
				errors = append(errors, err)
			}
		}
	}
	return errors
}

func (instance *Connections) CloseDB(db *gorm.DB) error {
	return closeDB(db)
}

func (instance *Connections) CloseSQL(db *sql.DB) error {
	return closeSQL(db)
}

func (instance *Connections) ConnectionNames(tag string) (names []string) {
	names = make([]string, 0)
	for name, info := range instance.settings {
		if len(tag) == 0 || tag == info.Tag {
			names = append(names, name)
		}
	}
	return
}

func (instance *Connections) EnsureConnection(conn *DbConfigConnection) bool {
	if nil != instance && nil != conn {
		uid := conn.Uid
		if !instance.existsConnection(uid) {
			instance.addConnection(uid, false)
			return true
		}
	}
	return false
}

// AddConnection Add a connection to internal storage.
// Supported data = (string, *Connection, *DbConfigConnection)
func (instance *Connections) AddConnection(args ...interface{}) {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	if len(args) > 2 {
		uid, driver, dsn, tag := getArgs(args...)
		instance.addConnectionDetails(uid, driver, dsn, tag, false)
	} else {
		instance.addConnection(args[0], false)
	}
}

func (instance *Connections) SetConnection(args ...interface{}) {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	if len(args) > 2 {
		uid, driver, dsn, tag := getArgs(args...)
		instance.addConnectionDetails(uid, driver, dsn, tag, true)
	} else {
		instance.addConnection(args[0], true)
	}
}

func (instance *Connections) GetConnection(uid string) *Connection {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	if conn, ok := instance.connections[uid]; !ok || nil == conn {
		root, driver, dsn := instance.splitUID(uid)
		instance.connections[uid] = NewConnection(instance.mode, instance, root, driver, dsn)
	}
	return instance.connections[uid]
}

// GetConnectionByName return a connection using the configuration name
func (instance *Connections) GetConnectionByName(name string) *Connection {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	if dbSettings, ok := instance.settings[name]; ok {
		root := instance.absolutizeRoot(dbSettings.Root)
		driver := dbSettings.Driver
		dsn := dbSettings.Dsn
		uid := instance.concatUID(root, driver, dsn)

		if conn, ok := instance.connections[uid]; !ok || conn == nil {
			instance.connections[uid] = NewConnection(instance.mode, instance, root, driver, dsn)
		}
		return instance.connections[uid]
	} else {
		// settings not found
		return nil
	}
}

func (instance *Connections) Get(name string) (*Connection, *gorm.DB, error) {
	conn := instance.GetConnectionByName(name)
	if nil != conn {
		db, err := conn.Get()
		if nil != err {
			_ = conn.Close()
			conn = nil
			db = nil
		}
		return conn, db, err
	}
	return nil, nil, gg.Errors.Prefix(ggxcommons.RuntimeError,
		fmt.Sprintf("Database configuration '%s' not found", name))
}

func (instance *Connections) Volatile(name string) (*Connection, *gorm.DB, error) {
	conn := instance.GetConnectionByName(name)
	if nil != conn {
		db, err := conn.Volatile()
		if nil != err {
			_ = conn.VolatileClose(db)
			conn = nil
			db = nil
		}
		return conn, db, err
	}
	return nil, nil, gg.Errors.Prefix(ggxcommons.RuntimeError,
		fmt.Sprintf("Database configuration '%s' not found", name))
}

func (instance *Connections) Close(item interface{}) error {
	instance.mux.Lock()
	defer instance.mux.Unlock()

	var uid string
	if s, ok := item.(string); ok {
		uid = s
	} else if c, ok := item.(*Connection); ok {
		uid = c.uid
	}
	if len(uid) > 0 {
		if _, ok := instance.connections[uid]; ok {
			conn := instance.connections[uid]

			instance.connections[uid] = nil
			delete(instance.connections, uid)

			return conn.close()
		}
	} else {
		return gg.Errors.Prefix(ggxcommons.PanicSystemError, "You cannot close a native database with this method but only managed connections. Use ggxserve_database_pool.CloseDB method instead.")
	}
	return nil
}

func (instance *Connections) absolutizeRoot(root string) string {
	if len(root) == 0 {
		root = instance.root
	} else {
		root = gg.Paths.Absolutize(root, instance.root)
	}
	return root
}

func (instance *Connections) concatUID(root, driver, dsn string) string {
	return fmt.Sprintf("%s|%s|%s", root, driver, dsn)
}

func (instance *Connections) splitUID(uid string) (root, driver, dsn string) {
	tokens := strings.Split(uid, "|")
	if len(tokens) == 3 {
		root = tokens[0]
		driver = tokens[1]
		dsn = tokens[2]
	}
	return
}

func (instance *Connections) addConnectionDetails(uid, driver, dsn, tag string, overwrite bool) {
	config := &DbConfigConnection{
		Root:   instance.root,
		Uid:    uid,
		Driver: driver,
		Dsn:    dsn,
		Tag:    tag,
	}
	instance.addConnection(config, overwrite)
}

func (instance *Connections) addConnection(item interface{}, overwrite bool) {
	if conn, ok := item.(*Connection); ok {
		uid := conn.uid
		if !instance.existsConnection(uid) || overwrite {
			instance.connections[uid] = conn
			instance.settings[uid] = &DbConfigConnection{
				Root:   instance.root,
				Uid:    uid,
				Driver: conn.driver,
				Dsn:    conn.dsn,
			}
		}
	} else if config, ok := item.(*DbConfigConnection); ok {
		root := instance.absolutizeRoot(config.Root)
		driver := config.Driver
		dsn := config.Dsn
		uid := instance.concatUID(root, driver, dsn)
		if !instance.existsConnection(uid) || overwrite {
			conn := NewConnection(instance.mode, instance, root, driver, dsn)
			instance.connections[uid] = conn
			instance.settings[config.Uid] = config
		}
	} else if str, ok := item.(string); ok {
		var config *DbConfigConnection
		err := gg.JSON.Read(str, &config)
		if nil == err && nil != config && len(config.Dsn) > 0 {
			instance.addConnection(config, false) // recursive
		}
	}
}

func (instance *Connections) existsConnection(uid string) (response bool) {
	_, response = instance.connections[uid]
	return
}

type Connection struct {
	mode   string
	pool   *Connections
	uid    string
	root   string
	driver string
	dsn    string
	_db    *gorm.DB
}

func NewConnection(mode string, pool *Connections, root, driver, dsn string) *Connection {
	instance := new(Connection)
	instance.mode = mode
	instance.pool = pool
	instance.uid = gg.Coding.MD5(root + driver + dsn)
	instance.root = root
	instance.driver = driver
	instance.dsn = dsn
	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Connection) Close() error {
	if nil != instance {
		if nil != instance.pool {
			return instance.pool.Close(instance.uid)
		}
		return instance.close()
	}
	return nil
}

func (instance *Connection) Get() (*gorm.DB, error) {
	var db *gorm.DB
	var err error
	if nil == instance._db {
		db, err = instance.build()
		if nil == err {
			instance._db = db
		}
	}
	return instance._db, err
}

func (instance *Connection) Volatile() (*gorm.DB, error) {
	return instance.build()
}

func (instance *Connection) VolatileClose(db *gorm.DB) error {
	return closeDB(db)
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *Connection) close() (err error) {
	if nil != instance && nil != instance._db {
		err = closeDB(instance._db)
		instance._db = nil
	}
	return
}

func (instance *Connection) closeDB(db *gorm.DB) error {
	return closeDB(db)
}

func (instance *Connection) closeSQL(db *sql.DB) error {
	return closeSQL(db)
}

func (instance *Connection) build() (db *gorm.DB, err error) {
	root := instance.root
	driver := instance.driver
	dsn := instance.dsn
	db, err = Build(root, driver, dsn, instance.mode)
	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	S T A T I C
// ---------------------------------------------------------------------------------------------------------------------

func Build(root, driver, dsn string, configOrMode interface{}) (db *gorm.DB, err error) {
	config := gormConfig(ggxcommons.ModeDebug)
	if nil != configOrMode {
		if s, ok := configOrMode.(string); ok {
			config = gormConfig(s)
		}
		if c, ok := configOrMode.(*gorm.Config); ok {
			config = c
		}
	}
	switch driver {
	case "sqlite":
		filename := gg.Paths.Absolutize(dsn, root)
		_ = gg.Paths.Mkdir(filename)
		if err != nil {
			return nil, err
		}
		db, err = gorm.Open(sqlite.Open(filename), config)
	case "mysql":
		// "user:pass@tcp(127.0.0.1:3306)/dbname?charset=utf8mb4&parseTime=True&loc=Local"
		db, err = gorm.Open(mysql.Open(dsn), config)
	case "postgres":
		// "host=localhost user=gorm password=gorm dbname=gorm port=9920 sslmode=disable TimeZone=Asia/Shanghai"
		db, err = gorm.Open(postgres.Open(dsn), config)
	case "sqlserver":
		// "sqlserver://gorm:LoremIpsum86@localhost:9930?database=gorm"
		db, err = gorm.Open(sqlserver.Open(dsn), config)
	default:
		db = nil
		err = gg.Errors.Prefix(ggxcommons.RuntimeError,
			fmt.Sprintf("Database '%s' not supported: ", driver))
	}
	return
}

func gormConfig(mode string) (config *gorm.Config) {
	if mode == ggxcommons.ModeProduction {
		config = &gorm.Config{
			Logger: logger.Default.LogMode(logger.Silent),
		}
	} else {
		config = &gorm.Config{
			Logger: logger.Default.LogMode(logger.Silent),
		}
	}
	return
}

func closeDB(db *gorm.DB) error {
	if nil != db {
		c, err := db.DB()
		if nil != err {
			return err
		}
		return closeSQL(c)
	}
	return nil
}

func closeSQL(db *sql.DB) error {
	if nil != db {
		return db.Close()
	}
	return nil
}

func getArgs(args ...interface{}) (uid, driver, dsn, tag string) {
	uid = gg.Convert.ToString(gg.Arrays.GetAt(args, 0, ""))
	driver = gg.Convert.ToString(gg.Arrays.GetAt(args, 1, ""))
	dsn = gg.Convert.ToString(gg.Arrays.GetAt(args, 2, ""))
	tag = gg.Convert.ToString(gg.Arrays.GetAt(args, 3, ""))
	return
}
