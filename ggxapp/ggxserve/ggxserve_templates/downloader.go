package ggxserve_templates

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"fmt"
)

/**
Download templates from GitHub or Bitbucket
*/

const (
	repoRoot = "https://bitbucket.org/digi-sense/gg-xapp/raw/master/ggxresources"
)

// ---------------------------------------------------------------------------------------------------------------------
// 	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxTemplates) DownloadTemplates() ([]string, []error) {
	// download
	session := gg.IO.NewDownloadSession(instance.templateActions())
	return session.DownloadAll(false)
}

// ---------------------------------------------------------------------------------------------------------------------
// 	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxTemplates) templateActions() []*gg_utils.DownloaderAction {
	response := make([]*gg_utils.DownloaderAction, 0)

	// email
	/*
		response = append(response, instance.actionOf("email", "password-reset.html.htm"))
		response = append(response, instance.actionOf("email", "password-reset.text.txt"))
		response = append(response, instance.actionOf("email", "password-reset.subject.txt"))
		response = append(response, instance.actionOf("email", "verify.html.htm"))
		response = append(response, instance.actionOf("email", "verify.text.txt"))
		response = append(response, instance.actionOf("email", "verify.subject.txt"))
		response = append(response, instance.actionOf("email", "welcome.html.htm"))
		response = append(response, instance.actionOf("email", "welcome.text.txt"))
		response = append(response, instance.actionOf("email", "welcome.subject.txt"))
	*/

	// sms
	// response = append(response, instance.actionOf("sms", "verify.text.txt"))

	// html
	response = append(response, instance.actionOf("html", "error.html"))
	// response = append(response, instance.actionOf("html", "password-reset.html"))
	// response = append(response, instance.actionOf("html", "verified.html"))

	return response
}

func (instance *GGxTemplates) actionOf(category, name string) *gg_utils.DownloaderAction {
	instance.Register(category, name)
	remotePath := fmt.Sprintf("%s/%s/%s", repoRoot, category, name)
	return gg.IO.NewDownloaderAction(
		"",
		remotePath,
		"",
		gg.Paths.ConcatDir(instance.root, category),
	)
}
