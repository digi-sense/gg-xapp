package ggxserve_templates

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-core/gg_utils"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxcommons"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxinitializer"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp/ggxserve/ggxserve_templates/html"
	"fmt"
	"github.com/cbroglie/mustache"
	"strings"
)

type GGxTemplates struct {
	root        string
	initializer *ggxinitializer.GGxInitializer
	events      *gg_events.Emitter

	payload   map[string]interface{}
	templates map[string]string
}

func NewGGxTemplates(baseAppURL, baseApiURL string, events *gg_events.Emitter, initializer *ggxinitializer.GGxInitializer) (instance *GGxTemplates) {
	instance = new(GGxTemplates)
	instance.root = gg.Paths.WorkspacePath(gg.Paths.NormalizePathForOS("./templates"))
	instance.events = events
	instance.initializer = initializer
	instance.payload = make(map[string]interface{})
	instance.templates = make(map[string]string)

	instance.payload["api-base-url"] = baseApiURL
	instance.payload["app-base-url"] = baseAppURL

	_ = instance.init()

	return
}

//----------------------------------------------------------------------------------------------------------------------
//	p u b l i c
//----------------------------------------------------------------------------------------------------------------------

func (instance *GGxTemplates) Root() string {
	if nil != instance {
		return instance.root
	}
	return ""
}

func (instance *GGxTemplates) GetPath(relative string) (response string) {
	if nil != instance {
		response = gg.Paths.Absolutize(relative, instance.root)
		_ = gg.Paths.Mkdir(response)
	}
	return
}

func (instance *GGxTemplates) ListFiles(relative string) (response []string) {
	if nil != instance {
		dir := instance.GetPath(relative)
		response, _ = gg.Paths.ListFiles(dir, "*.*")
	}
	return
}

func (instance *GGxTemplates) GetBaseAppURL() string {
	if nil != instance {
		return gg.Maps.GetString(instance.payload, "app-base-url")
	}
	return ""
}

func (instance *GGxTemplates) GetBaseApiURL() string {
	if nil != instance {
		return gg.Maps.GetString(instance.payload, "api-base-url")
	}
	return ""
}

// Payload static payload for templates
func (instance *GGxTemplates) Payload() map[string]interface{} {
	if nil != instance {
		return instance.payload
	}
	return make(map[string]interface{})
}

func (instance *GGxTemplates) Register(category, name string) string {
	ext := gg.Paths.Extension(name)
	if len(ext) > 0 {
		i := strings.LastIndex(name, ext)
		name = name[:i]
	}
	uid := fmt.Sprintf("%s.%s", category, name)
	// category/name.ext
	fileName := gg.Paths.Concat(instance.root, fmt.Sprintf("%s%s%s%s", category, gg_utils.OS_PATH_SEPARATOR, name, ext))
	// add to internal storage
	instance.templates[uid] = fileName
	return fileName
}

// Deploy register and deploy a file template
// name is full name of file "file.html.htm"
func (instance *GGxTemplates) Deploy(category, name, content string) error {
	fileName := instance.Register(category, name)
	// save
	return instance.initializer.Deploy(fileName, content)
}

// DeployEmailTemplate register and deploy 3 files
// templateName is name of template i.e. "password-reset"
func (instance *GGxTemplates) DeployEmailTemplate(category, templateName, subject, html, text string) (err error) {
	subjectName := fmt.Sprintf("%s.subject.txt", templateName)
	subjectFile := instance.Register(category, subjectName)
	err = instance.initializer.Deploy(subjectFile, subject)
	if nil != err {
		return
	}

	htmlName := fmt.Sprintf("%s.html.htm", templateName)
	htmlFile := instance.Register(category, htmlName)
	err = instance.initializer.Deploy(htmlFile, html)
	if nil != err {
		return
	}

	textName := fmt.Sprintf("%s.text.txt", templateName)
	textFile := instance.Register(category, textName)
	err = instance.initializer.Deploy(textFile, text)
	if nil != err {
		return
	}

	return
}

func (instance *GGxTemplates) Get(category, name string) string {
	if nil != instance {
		content, _ := instance.read(category, name)
		return content
	}
	return ""
}

// Render
// render a template
// sample: Render('email', 'welcome.html')
func (instance *GGxTemplates) Render(category, name string, context ...interface{}) (string, error) {
	if nil != instance {
		content, err := instance.read(category, name)
		if nil != err {
			return "", err
		}
		if len(content) > 0 {
			payload := instance.data(context...)
			return mustache.Render(content, payload)
		}
	}
	return "", nil
}

//----------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
//----------------------------------------------------------------------------------------------------------------------

func (instance *GGxTemplates) init() (err error) {
	_ = gg.Paths.Mkdir(instance.root)

	// deploy 404
	err = instance.Deploy("html", "tpl_404.html", html.Tpl404)

	// download and register other templates
	instance.DownloadTemplates()

	return
}

func (instance *GGxTemplates) read(category, name string) (content string, err error) {
	uid := fmt.Sprintf("%s.%s", category, name)
	if fileName, ok := instance.templates[uid]; ok {
		content, err = gg.IO.ReadTextFromFile(fileName)
	} else {
		err = gg.Errors.Prefix(ggxcommons.RuntimeError, "Template does not exists")
	}
	return
}

func (instance *GGxTemplates) data(context ...interface{}) map[string]interface{} {
	// creates data with payload
	data := map[string]interface{}{}
	for k, v := range instance.payload {
		data[k] = v
	}

	if len(context) > 0 {
		for _, c := range context {
			if payload, ok := c.(map[string]interface{}); ok {
				for k, v := range payload {
					if k == "link" {
						url := gg.Convert.ToString(v)
						if strings.HasPrefix(url, "./") {
							v = gg.Paths.Concat(gg.Reflect.GetString(instance.payload, "api-base-url"), url)
						} else {
							v = url
						}
					}
					data[k] = v
				}
			}
		}
	}
	return data
}
