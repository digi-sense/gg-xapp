package ggxapp

import (
	"bitbucket.org/digi-sense/gg-core"
	"bitbucket.org/digi-sense/gg-core/gg_events"
	"bitbucket.org/digi-sense/gg-core/gg_ticker"
	"sync"
	"time"
)

// ---------------------------------------------------------------------------------------------------------------------
//		t y p e
// ---------------------------------------------------------------------------------------------------------------------

type FileMonitor struct {
	roots      []string // where stop file is stored
	fileName   string
	eventName  string
	events     *gg_events.Emitter
	fileMux    sync.Mutex
	stopTicker *gg_ticker.Ticker
}

// ---------------------------------------------------------------------------------------------------------------------
//		c o n s t r u c t o r
// ---------------------------------------------------------------------------------------------------------------------

func NewFileMonitor(roots []string, fileName, eventName string, events *gg_events.Emitter) *FileMonitor {
	instance := new(FileMonitor)
	instance.roots = roots
	instance.fileName = fileName
	instance.eventName = eventName
	instance.events = events

	return instance
}

// ---------------------------------------------------------------------------------------------------------------------
//		p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *FileMonitor) Start() {
	if nil != instance && len(instance.fileName) > 0 && nil == instance.stopTicker {
		instance.stopTicker = gg_ticker.NewTicker(1*time.Second, func(t *gg_ticker.Ticker) {
			instance.checkFile()
			// instance.logger.Debug("Checking for stop command....")
		})
		instance.stopTicker.Start()
	}
}

func (instance *FileMonitor) Stop() {
	if nil != instance && nil != instance.stopTicker {
		instance.stopTicker.Stop()
		instance.stopTicker = nil
	}
}

// ---------------------------------------------------------------------------------------------------------------------
//		p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func (instance *FileMonitor) checkFile() {
	if nil != instance && len(instance.fileName) > 0 {
		instance.fileMux.Lock()
		defer instance.fileMux.Unlock()

		// check if file exists
		for _, root := range instance.roots {
			cmdFile := gg.Paths.Concat(root, instance.fileName)
			if b, _ := gg.Paths.Exists(cmdFile); b {
				_ = gg.IO.Remove(cmdFile)
				instance.events.EmitAsync(instance.eventName)
			}
		}
	}
}
