import smtplib
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from os.path import basename

# variables
host = '{{ .host }}'
port = '{{ .port }}'
username = '{{ .user }}'
password = '{{ .password }}'
address_from = '{{ .from }}'
address_reply_to = '{{ .reply_to }}'
address_to = '{{ .to }}'
address_cc = '{{ .cc }}'
address_bcc = '{{ .bcc }}'
subject = "{{ .subject }}"
text = """\
{{ .text }}"""
html = """\
{{ .html }}
"""
attachments = "{{ .attachments }}"
files = attachments.split(",")

# initialize connection to our email server
smtp = smtplib.SMTP(host, port=port)
smtp.ehlo()  # send the extended hello to our server
smtp.starttls()  # tell server we want to communicate with TLS encryption
smtp.login(username, password)  # login to our email server

message = MIMEMultipart("alternative")
message["Subject"] = subject
message["From"] = address_from
message["ReplyTo"] = address_reply_to
message["To"] = address_to
message["Cc"] = address_cc
message["Bcc"] = address_bcc

# Turn these into plain/html MIMEText objects
part1 = MIMEText(text, "plain")
part2 = MIMEText(html, "html")

# Add HTML/plain-text parts to MIMEMultipart message
# The email client will try to render the last part first
message.attach(part1)
message.attach(part2)

# attachments
for f in files or []:
    try:
        with open(f, "rb") as fil:
            part = MIMEApplication(
                fil.read(),
                Name=basename(f)
            )
        # After the file is closed
        part['Content-Disposition'] = 'attachment; filename="%s"' % basename(f)
        message.attach(part)
    except Exception:
        pass

# send our email message 'msg' to our boss
smtp.sendmail(
    address_from, address_to, message.as_string()
)

smtp.quit()  # close the connection
