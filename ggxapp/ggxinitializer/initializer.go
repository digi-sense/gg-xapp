package ggxinitializer

import (
	"bitbucket.org/digi-sense/gg-core"
	_ "embed"
	"fmt"
)

// ---------------------------------------------------------------------------------------------------------------------
//	s e t t i n g s
// ---------------------------------------------------------------------------------------------------------------------

//go:embed tpl_settings.json
var TplSettings string

//go:embed tpl_webserver.json
var TplWebserver string

//go:embed tpl_database.json
var TplDatabase string

//go:embed tpl_postman.json
var TplPostman string

//go:embed tpl_script_send_email.py
var TplScriptSendEmail string

// ---------------------------------------------------------------------------------------------------------------------
//	init
// ---------------------------------------------------------------------------------------------------------------------

type GGxInitializer struct {
}

func NewGGxInitializer() (instance *GGxInitializer) {
	instance = new(GGxInitializer)

	return
}

// ---------------------------------------------------------------------------------------------------------------------
//	p u b l i c
// ---------------------------------------------------------------------------------------------------------------------

func (instance *GGxInitializer) Initialize(mode string) (err error) {
	tmpRoot := gg.Paths.WorkspacePath("temp")
	gg.Paths.SetTempRoot(tmpRoot)
	gg.IO.RemoveAllSilent(tmpRoot)

	// settings.mode.json
	err = instance.DeployIntoWorkspace(fmt.Sprintf("settings.%s.json", mode), TplSettings)
	if nil != err {
		return
	}

	// webserver.mode.json
	err = instance.DeployIntoWorkspace(fmt.Sprintf("webserver.%s.json", mode), TplWebserver)
	if nil != err {
		return
	}

	// database.mode.json
	err = instance.DeployIntoWorkspace(fmt.Sprintf("database.%s.json", mode), TplDatabase)
	if nil != err {
		return
	}

	// postman.mode.json
	err = instance.DeployIntoWorkspace(fmt.Sprintf("postman.%s.json", mode), TplPostman)
	if nil != err {
		return
	}

	// scripts/send_email.py
	err = instance.DeployIntoWorkspace("./scripts/send_email.py", TplScriptSendEmail)
	if nil != err {
		return
	}

	return
}

func (instance *GGxInitializer) DeployIntoWorkspace(name string, tpl string) error {
	filename := gg.Paths.WorkspacePath(name)
	return write(filename, tpl)
}

func (instance *GGxInitializer) DeployBytesIntoWorkspace(name string, data []byte) error {
	filename := gg.Paths.WorkspacePath(name)
	return writeBytes(filename, data)
}

func (instance *GGxInitializer) Deploy(fullName string, tpl string) error {
	return write(fullName, tpl)
}

func (instance *GGxInitializer) DeployBytes(fullName string, data []byte) error {
	return writeBytes(fullName, data)
}

// ---------------------------------------------------------------------------------------------------------------------
//	p r i v a t e
// ---------------------------------------------------------------------------------------------------------------------

func write(filename, content string) (err error) {
	if b, _ := gg.Paths.Exists(filename); !b {
		_ = gg.Paths.Mkdir(filename)
		_, err = gg.IO.WriteTextToFile(content, filename)
	}
	return
}

func writeBytes(filename string, data []byte) (err error) {
	if b, _ := gg.Paths.Exists(filename); !b {
		_ = gg.Paths.Mkdir(filename)
		_, err = gg.IO.WriteBytesToFile(data, filename)
	}
	return
}
