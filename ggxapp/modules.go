package ggxapp

import (
	"bitbucket.org/digi-sense/gg-core"
	"fmt"
	"strings"
	"sync"
)

type ModuleCallback func() (err error)

type ApplicationModule struct {
	Name    string `json:"name"`
	Version string `json:"version"`
	Start   ModuleCallback
	Stop    ModuleCallback
}

func (instance *ApplicationModule) String() string {
	return gg.JSON.Stringify(instance)
}

type ApplicationModules struct {
	mux  sync.Mutex
	data []*ApplicationModule
}

func NewApplicationModules() (instance *ApplicationModules) {
	instance = new(ApplicationModules)
	instance.data = make([]*ApplicationModule, 0)
	return
}

func (instance *ApplicationModules) AddModule(name, version string, start, stop ModuleCallback) {
	if nil != instance && len(name) > 0 && len(version) > 0 {
		instance.Add(&ApplicationModule{
			Name:    name,
			Version: version,
			Start:   start,
			Stop:    stop,
		})
	}
}

func (instance *ApplicationModules) Add(module *ApplicationModule) {
	if nil != instance && nil != module && len(module.Name) > 0 && !instance.Has(module.Name) {
		instance.mux.Lock()
		defer instance.mux.Unlock()
		instance.data = append(instance.data, module)
	}
}

func (instance *ApplicationModules) Has(name string) bool {
	if nil != instance {
		for _, item := range instance.data {
			if item.Name == name {
				return true
			}
		}
	}
	return false
}

func (instance *ApplicationModules) Start() (errs []error) {
	if nil != instance && nil != instance.data {
		for _, item := range instance.data {
			if nil != item.Start {
				errs = append(errs, item.Start())
			}
		}
	}
	return
}

func (instance *ApplicationModules) Stop() (errs []error) {
	if nil != instance && nil != instance.data {
		for _, item := range instance.data {
			if nil != item.Stop {
				errs = append(errs, item.Stop())
			}
		}
	}
	return
}

func (instance *ApplicationModules) Info() string {
	if nil != instance {
		if len(instance.data) == 0 {
			return "No external modules are in use."
		} else {
			buff := new(strings.Builder)
			buff.WriteString("Using modules [")
			for i, item := range instance.data {
				if i > 0 {
					buff.WriteString(",")
				}
				buff.WriteString(fmt.Sprintf("%s@v%s", item.Name, item.Version))
			}
			buff.WriteString("]")
			return buff.String()
		}
	}
	return ""
}
