package main

import (
	"bitbucket.org/digi-sense/gg-core"
	_ "bitbucket.org/digi-sense/gg-core-x"
	"bitbucket.org/digi-sense/gg-xapp/ggxapp"
	"flag"
	_ "github.com/gofiber/fiber/v2"
	_ "gorm.io/driver/mysql"
	_ "gorm.io/driver/postgres"
	_ "gorm.io/driver/sqlite"
	_ "gorm.io/driver/sqlserver"
	_ "gorm.io/gorm"
	"log"
	"os"
)

func init() {
	gg.AppName = "TEST APPLICATION"
}

// Program arguments: run -dir_work=./test/_workspace
func main() {
	// PANIC RECOVERY
	defer gg.Recover("main()")

	//-- command flags --//
	// run
	cmdRun := flag.NewFlagSet("run", flag.ExitOnError)
	dirWork := cmdRun.String("dir_work", gg.Paths.Absolute("./_workspace"), "Set a particular folder as main workspace")
	mode := cmdRun.String("m", gg.ModeProduction, "Mode allowed: 'debug' or 'production'")

	// parse
	if len(os.Args) > 1 {
		cmd := os.Args[1]
		switch cmd {
		case "run":
			_ = cmdRun.Parse(os.Args[2:])
		default:
			panic("Command not supported: " + cmd)
		}
	} else {
		panic("Missing command. i.e. 'run'")
	}

	app, err := ggxapp.NewApplicationBuilder(*mode, *dirWork)
	if nil == err {
		runtime, err := app.Build()
		if nil == err {
			err = runtime.Start()
			if nil != err {
				log.Panicf("Error starting %s: %s", gg.AppName, err.Error())
			} else {
				// app is running
				runtime.Join()
			}
		} else {
			log.Panicf("Error launching %s: %s", gg.AppName, err.Error())
		}
	} else {
		log.Panicf("Error launching %s: %s", gg.AppName, err.Error())
	}

}
